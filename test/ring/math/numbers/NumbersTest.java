package ring.math.numbers;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import static ring.formatting.FormatSpecifier.noWidth;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

import ring.formatting.FormatSpecifier;

public class NumbersTest {
	static final ErrorMessages errors = new ErrorMessages();
	
	public static void main(String[] args) {
		if(Boolean.FALSE) checkDoubleConversion(1000000);
		if(Boolean.FALSE) checkStringConversion(1000000);
		System.out.println(new u4(-1));
		System.out.println(new u4(-1).convertTo(i4.class));
		System.out.println(new u4(-1).convertToLossless(i4.class));
		System.out.println(new u4(-1).convertToRaw(i4.class));
		var v1 = 1L<<40;
		var v2 = new i8(1L<<40).convertTo(f4.class).rawValue();
		System.out.println(v1+", "+v2+", "+new bigfloat(v2));
		if(v1 != v2) throw new Error(errors.unreachable);
		var l = new ArrayList<String>();
		for(var e : ContainedClasses._bigfloat) l.add(e.getSimpleName());
		l.sort(Comparator.comparing(e->e.toLowerCase()));
		System.out.println(l);
	}
	
	private static void checkDoubleConversion(int rounds) {
		var rand = new Random(0);
		for(int i = 0; i < rounds; i++) {
			var l1 = rand.nextLong();
			var d1 = Double.longBitsToDouble(l1);
			var f = new bigfloat(d1);
			var d2 = f.roundToF8().rawValue();
			var l2 = Double.doubleToRawLongBits(d2);
			if(Double.isNaN(d1) && Double.isNaN(d2)) continue;
			if(l1 != l2) throw new Error(errors.unreachable);
		}
	}
	
	private static void checkStringConversion(int rounds) {
		var t1 = System.nanoTime();
		var rand = new Random(0);
		for(int i = 0, i0 = 0; i < rounds; i++) {
			var d = Double.longBitsToDouble(rand.nextLong());
			var f1 = new bigfloat(d);
			int radix = 0;
			switch(rand.nextInt(4)) {
			case 0: radix = 2; break;
			case 1: radix = 10; break;
			case 2: radix = 16; break;
			case 3: radix = rand.nextInt(num.maxRadix-1)+2; break;
			}
			var fmt = new FormatSpecifier(0,Collections.emptySet(),radix,noWidth,noWidth);
			var str1 = f1.toString(fmt);
			var f2 = bigfloat.parse(str1);
			var str2 = f2.toString(fmt);
			var out = String.format("%e:\t%s\t%s\t\tradix %d:\t%s\t%s\n",d,f1,f2,radix,str1,str2);
			if(Boolean.TRUE) {
				var t2 = System.nanoTime();
				if(t2-t1 >= 1000000000) {
					System.out.println((i-i0)*1000000000D/(t2-t1)+" rounds/sec");
					i0 = i; t1 = t2;
				}
			}
			if(Boolean.FALSE) System.out.print(out);
			//if(!f1.equals(f2)) throw new Error(errors.unreachable); //almost, but no
		}
	}
}
