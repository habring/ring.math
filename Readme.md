# Ring.Math

This library provides support for huge floating point numbers (with "unlimited" precision, like java.math.BigDecimal, but better).  
Converting between number types is more consistent in this library than in Java's default implementation.  
A well thought out re-implementation of primitive number types is also included (with optional overflow-checking).  
Furthermore, there are unsigned numbers and a re-implementation of java.math.BigInteger.  
Number formatting includes more options than in the Java standard library.

# Dependencies

Ring.Math has no dependencies.

# Copyright

Copyright 2021 Lukas Habring

For commercial licenses, just contact Lukas Habring: <ring.commercial@gmx.at>  
This file is part of Ring.Math.

Ring.Math is free software: you can redistribute it and/or modify  
it under the terms of the GNU Affero General Public License version 3  
(and only version 3) as published by the Free Software Foundation.

Ring.Math is distributed in the hope that it will be useful,  
but WITHOUT ANY WARRANTY; without even the implied warranty of  
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the  
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License  
along with Ring.Math. If not, see <http://www.gnu.org/licenses/>.
