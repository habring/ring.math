package ring.util;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.*;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Map.Entry;

import ring.util.Converter.MultiConverter;

public final class Converters {
	static final ErrorMessages errors = new ErrorMessages();
	private final Map<Entry<Class<?>,Class<?>>,Converter<?,?>> data;
	
	public Converters() { this(Collections.emptyMap()); }
	
	public Converters(Collection<Converter<?,?>> c) { this(new Converters().appendAll(c).data); }
	
	private Converters(Map<Entry<Class<?>,Class<?>>,Converter<?,?>> data) {
		this.data = Collections.unmodifiableMap(new HashMap<>(data));
		for(var e : this.data.keySet()) {
			if(!this.data.containsKey(e(e.getValue(),e.getKey()))) {
				throw new Error(errors.unreachable);
			}
		}
	}
	
	/**Returns a new {@link Converters}-object that includes the given {@link Converter}.<br>
	 * {@link Converter}s that are added first have priority over the ones added later
	 * when there are two conversion paths with the same length.<br>
	 * E.g. lets assume that type A can be converted to B via C or D. If the conversion
	 * A->C->B is possible first, because the converter from D to B was not added yet,
	 * then the conversion from A to B will always be performed via C and not via D.*/
	public Converters append(Converter<?,?> conv) { return appendAll(List.of(conv)); }
	
	public Converters appendAll(Collection<Converter<?,?>> c) {
		var todo = new ArrayList<>(c);
		for(int i = 0, size = todo.size(); i < size; i++) {
			var e = todo.get(i);
			if(e.fromType == e.toType) throw new IllegalArgumentException("from type == to type");
			todo.add(e.inverse());
		}
		var data = new HashMap<>(this.data);
		for(var e : todo) {
			if(!add0(data,e)) throw new IllegalArgumentException("conflicting converters");
			add0(data,Converter.identity(e.fromType));
			add0(data,Converter.identity(e.toType));
		}
		var fromMap = new HashMap<Class<?>,List<Converter<?,?>>>();
		var toMap = new HashMap<Class<?>,List<Converter<?,?>>>();
		for(var e : data.values()) {
			if(e.fromType == e.toType) continue;
			fromMap.computeIfAbsent(e.fromType,k->new ArrayList<>()).add(e);
			toMap.computeIfAbsent(e.toType,k->new ArrayList<>()).add(e);
		}
		while(todo.size() > 0) {
			var conv2 = todo.remove(todo.size()-1);
			for(var e : toMap.get(conv2.fromType)) {
				var conv3 = concat(e,conv2);
				if(add0(data,conv3)) todo.add(conv3);
			}
			for(var e : fromMap.get(conv2.toType)) {
				var conv3 = concat(conv2,e);
				if(add0(data,conv3)) todo.add(conv3);
			}
		}
		return new Converters(data);
	}
	
	private static boolean add0(Map<Entry<Class<?>,Class<?>>,Converter<?,?>> data, Converter<?,?> conv) {
		var tmp = data.putIfAbsent(e(conv.fromType,conv.toType),conv);
		if(tmp == null || tmp == conv) return true;
		var ret = sizeOf(conv) < sizeOf(tmp);
		if(ret) data.put(e(conv.fromType,conv.toType),conv);
		return ret;
	}
	
	public <A,B> Converter<A,B> get(Class<A> fromType, Class<B> toType) {
		var tmp = getOrNull(fromType,toType);
		if(tmp != null) return tmp;
		throw new IllegalArgumentException("cannot convert from "+fromType+" to "+toType);
	}
	
	@SuppressWarnings("unchecked")
	public <A,B> Converter<A,B> getOrNull(Class<A> fromType, Class<B> toType) { return (Converter<A,B>)data.get(e(fromType,toType)); }
	
	public int size() { return data.size(); }
	
	public int maxPathLength() {
		int ret = 0;
		for(var e : data.values()) ret = Math.max(ret,sizeOf(e));
		return ret;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static Converter<?,?> concat(Converter<?,?> a, Converter<?,?> b) { return ((Converter)a).andThen(b); }
	
	private static int sizeOf(Converter<?,?> c) { return c instanceof MultiConverter<?,?> m ? m.l.size() : 1; }
	
	private static <K,V> Entry<K,V> e(K key, V value) { return new SimpleImmutableEntry<>(key,value); }
}
