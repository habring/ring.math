package ring.util;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import static ring.util.Converters.errors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public abstract class Converter<A,B> {
	public final Class<A> fromType;
	public final Class<B> toType;
	
	public Converter(Class<A> fromType, Class<B> toType) {
		this.fromType = Objects.requireNonNull(fromType);
		this.toType = Objects.requireNonNull(toType);
	}
	
	public abstract B convert(A obj);
	
	public abstract A revert(B obj);
	
	public static <T> Converter<T,T> identity(Class<T> type) { return new IdentConverter<>(type); }
	
	public final Converter<B,A> inverse() {
		return this instanceof InvConverter<A,B> conv ? conv.conv : new InvConverter<>(this);
	}
	
	public <C> Converter<A,C> andThen(Converter<B,C> conv) {
		if(toType != conv.fromType) throw new Error(errors.unreachable);
		var l = new ArrayList<Converter<?,?>>();
		if(this instanceof MultiConverter<?,?> m1) l.addAll(m1.l);
		else l.add(this);
		if(conv instanceof MultiConverter<?,?> m2) l.addAll(m2.l);
		else l.add(conv);
		return new MultiConverter<>(l);
	}
	
	private static class IdentConverter<T> extends Converter<T,T> {
		IdentConverter(Class<T> type) { super(type,type); }
		
		@Override public T convert(T obj) { return obj; }
		
		@Override public T revert(T obj) { return obj; }
	}
	
	private static class InvConverter<A,B> extends Converter<A,B> {
		final Converter<B,A> conv;
		
		InvConverter(Converter<B,A> conv) { super(conv.toType,conv.fromType); this.conv = conv; }
		
		@Override public B convert(A obj) { return conv.revert(obj); }
		
		@Override public A revert(B obj) { return conv.convert(obj); }
	}
	
	static class MultiConverter<A,B> extends Converter<A,B> {
		final List<Converter<?,?>> l;
		
		@SuppressWarnings("unchecked")
		MultiConverter(List<Converter<?,?>> l) {
			super((Class<A>)first(l=new ArrayList<>(l)).fromType,(Class<B>)last(l).toType);
			this.l = Collections.unmodifiableList(l);
		}
		
		private static <E> E first(List<E> l) { return l.get(0); }
		
		private static <E> E last(List<E> l) { return l.get(l.size()-1); }
		
		@Override
		@SuppressWarnings({ "unchecked", "rawtypes" })
		public B convert(A obj) {
			Object tmp = obj;
			for(var e : l) tmp = ((Converter)e).convert(tmp);
			return (B)tmp;
		}
		
		@Override
		@SuppressWarnings({ "unchecked", "rawtypes" })
		public A revert(B obj) {
			Object tmp = obj;
			for(int i = l.size()-1; i >= 0; i--) tmp = ((Converter)l.get(i)).revert(tmp);
			return (A)tmp;
		}
	}
}
