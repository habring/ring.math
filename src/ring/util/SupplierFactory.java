package ring.util;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.Objects;
import java.util.function.Supplier;

@SuppressWarnings("TODO internal api")
public final class SupplierFactory {
	public static Supplier<Character> create(String str) { return create(str,0,str.length()); }
	
	public static Supplier<Character> create(String str, int offset, int length) {
		Objects.requireNonNull(str);
		return new Supplier<>() {
			int i = offset-1;
			@Override public Character get() { i++; return i < offset+length ? str.charAt(i) : null; }
		};
	}
	
	public static <E> Supplier<E> create(Iterable<E> l) {
		var it = l.iterator();
		return () -> it.hasNext() ? it.next() : null;
	}
}
