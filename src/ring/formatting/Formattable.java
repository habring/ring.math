package ring.formatting;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import static ring.formatting.FormatSpecifier.defaultRadix;
import static ring.formatting.FormatSpecifier.noWidth;

import java.util.Formatter;

import ring.formatting.FormatSpecifier.Flag;

public interface Formattable extends java.util.Formattable {
	String toString(FormatSpecifier fmt);
	
	@Override
	default void formatTo(Formatter formatter, int flags, int width, int precision) {
		var mw = width < 0 ? noWidth : new WidthRange(width,-1);
		var fw = precision < 0 ? noWidth : new WidthRange(-1,precision);
		var fmt = new FormatSpecifier(0,Flag.fromJFlags(flags),defaultRadix,mw,fw);
		formatter.format("%s",toString(fmt));
	}
}
