package ring.formatting;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import static ring.formatting.FormatSpecifier.Flag.*;
import static ring.math.numbers.i4s.isDecimalDigit;
import static ring.math.numbers.i4s.parseDecimal;

import java.util.*;
import java.util.function.Supplier;

import ring.math.numbers.num;
import ring.util.SupplierFactory;

//http://www.cplusplus.com/reference/cstdio/printf
//https://docs.oracle.com/javase/7/docs/api/java/util/Formatter.html#syntax
//%[argument_index$][flags][width][.precision]conversion
public final class FormatSpecifier extends FormatString {
	public enum Flag {
		leftJustify, padZero,
		showSign, showSpaceSign,
		noScientificForm, scientificForm;
		
		static Collection<Flag> fromJFlags(int flags) { return jf2f[flags]; }
	}
	
	@SuppressWarnings("unchecked")
	private static final Collection<Flag>[] jf2f = new Collection[(FormattableFlags.LEFT_JUSTIFY|FormattableFlags.UPPERCASE|FormattableFlags.ALTERNATE)+1];
	
	static {
		for(int i = 0; i < jf2f.length; i++) {
			var tmp = EnumSet.noneOf(Flag.class);
			if((i&FormattableFlags.LEFT_JUSTIFY) != 0) tmp.add(leftJustify);
			//FormattableFlags.UPPERCASE is not supported and therefore ignored here
			if((i&FormattableFlags.ALTERNATE) != 0) tmp.add(scientificForm);
			jf2f[i] = Collections.unmodifiableSet(tmp);
		}
	}
	
	public static final WidthRange noWidth = new WidthRange(-1);
	public static final int defaultRadix = 10;
	public static final FormatSpecifier string = parse(SupplierFactory.create("%s"));
	public final Set<Flag> flags;
	public final int radix;
	public final WidthRange magWidth, fracWidth;
	public final WidthRange totalWidth;
	
	public FormatSpecifier(int argIndex, Collection<Flag> flags, int radix, WidthRange magWidth, WidthRange fracWidth) {
		super(argIndex);
		var tmp = EnumSet.noneOf(Flag.class); tmp.addAll(flags); this.flags = Collections.unmodifiableSet(tmp);
		this.radix = num.checkRadix(radix);
		this.magWidth = Objects.requireNonNull(magWidth); this.fracWidth = Objects.requireNonNull(fracWidth);
		this.totalWidth = magWidth.add(fracWidth);
		checkXorFlags(leftJustify,padZero); checkXorFlags(showSign,showSpaceSign); checkXorFlags(noScientificForm,scientificForm);
	}
	
	/**format: %([0-9]+$|)(-|+| |0|e|f)*[0-9]*(:[0-9]*|)(\.[0-9]*(:[0-9]*|)|)(b|(d|s)|h|r[0-9]+r<br>
	 * ebnf: "%" [arg_index "$"] [flags] [magWidth] ["." fracWidth] radix<br>
	 * width and precision ebnf: exactWidth | minWidth ":" maxWidth*/
	static FormatSpecifier parse(Supplier<Character> input) {
		var la = input.get();
		if(la != '%') throw new IllegalArgumentException("input does not start with \"%\"");
		int argIndex = noArgIndex; var flags = new ArrayList<Flag>(); int radix = 0;
		WidthRange magWidth = noWidth, fracWidth = noWidth;
		var digits = new ArrayList<Character>();
		la = input.get();
		while(isDecimalDigit(la)) { digits.add(la); la = input.get(); }
		int digitsNum = parseDecimal(SupplierFactory.create(digits)).rawValue();
		if(digits.size() > 0) {
			if(la == '$') { argIndex = digitsNum; la = input.get(); digits.clear(); }
			else if(digits.get(0) == '0') { digits.remove(0); flags.add(padZero); }
		}
		if(digits.size() == 0) {
			while(true) {
				var run = true;
				switch(la) {
				case '-': flags.add(leftJustify); break;
				case '+': flags.add(showSign); break;
				case ' ': flags.add(showSpaceSign); break;
				case 'f': flags.add(noScientificForm); break;
				case 'e': flags.add(scientificForm); break;
				default: run = false; break;
				}
				if(!run) break;
				la = input.get();
			}
			while(isDecimalDigit(la)) { digits.add(la); la = input.get(); }
			digitsNum = parseDecimal(SupplierFactory.create(digits)).rawValue();
		}
		if(la == ':') {
			la = input.get();
			while(isDecimalDigit(la)) { digits.add(la); la = input.get(); }
			magWidth = new WidthRange(digitsNum,parseDecimal(SupplierFactory.create(digits)).rawValue());
			digits.clear();
		}
		else if(digits.size() > 0) { magWidth = new WidthRange(digitsNum); digits.clear(); }
		if(la == '.') {
			la = input.get();
			while(isDecimalDigit(la)) { digits.add(la); la = input.get(); }
			digitsNum = parseDecimal(SupplierFactory.create(digits)).rawValue();
			if(la == ':') {
				while(isDecimalDigit(la)) { digits.add(la); la = input.get(); }
				fracWidth = new WidthRange(digitsNum,parseDecimal(SupplierFactory.create(digits)).rawValue());
			}
			else fracWidth = new WidthRange(digitsNum);
			digits.clear();
		}
		if(digits.size() != 0) throw new Error(errors.unreachable);
		switch(la) {
		case 'b': radix = 2; break;
		case 'd': case 's': radix = defaultRadix; break;
		case 'h': radix = 16; break;
		case 'r':
			la = input.get();
			while(isDecimalDigit(la)) { digits.add(la); la = input.get(); }
			radix = parseDecimal(SupplierFactory.create(digits)).rawValue();
			if(la != 'r') throw new IllegalArgumentException("missin 'r' at the end of custom radix");
			input.get();
			break;
		default: throw new IllegalArgumentException("invalid radix "+la);
		}
		return new FormatSpecifier(argIndex,flags,radix,magWidth,fracWidth);
	}
	
	@Override public int argCount() { return 1; }
	
	@Override protected String format0(Object[] args, int offset) { return format0(args[offset]); }
	
	private String format0(Object arg) {
		if(arg == null) return "null";
		if(arg instanceof Formattable f) return f.toString(this);
		if(arg instanceof Number n) {
			var tmp = num.wrapOrNull(n);
			if(tmp != null) return tmp.toString(this);
		}
		if(this.equals(string)) return arg.toString();
		throw new IllegalArgumentException("invalid format specifier for object of type "+arg.getClass());
	}
	
	@Override
	public String toString() {
		var b = new StringBuilder().append('%');
		if(argIndex != noArgIndex) b.append(argIndex).append('$');
		for(var flag : flags) b.append(flag);
		magWidth.appendTo(b);
		if(!fracWidth.equals(noWidth)) { b.append('.'); fracWidth.appendTo(b); }
		switch(radix) {
		case 2: b.append('b'); break;
		case defaultRadix: b.append('s'); break;
		case 16: b.append('h'); break;
		default: b.append('r').append(radix).append('r'); break;
		}
		return b.toString();
	}
	
	private void checkXorFlags(Flag f1, Flag f2) {
		if(this.flags.contains(f1) && this.flags.contains(f2)) {
			throw new IllegalArgumentException("the flags "+f1.name()+" and "+f2.name()+" cannot be used at the same time");
		}
	}
}
