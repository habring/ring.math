package ring.formatting;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class MergedFormatString extends FormatString {
	private final List<FormatString> data;
	private final int argCount;
	
	public MergedFormatString(List<FormatString> l) {
		super(noArgIndex);
		var data = new ArrayList<FormatString>();
		for(var e : l) {
			if(e instanceof MergedFormatString m) data.addAll(m.data);
			else data.add(Objects.requireNonNull(e));
		}
		data.trimToSize();
		this.data = Collections.unmodifiableList(data);
		int argCount = 0;
		for(var e : this.data) argCount += e.argCount();
		this.argCount = argCount;
	}
	
	public List<FormatString> elements() { return data; }
	
	@Override public int argCount() { return argCount; }
	
	@Override
	protected String format0(Object[] args, int offset) {
		var b = new StringBuilder(); int i = offset;
		for(var fmt : data) {
			if(fmt.argIndex != noArgIndex) { b.append(fmt.format0(args,fmt.argIndex)); }
			else { b.append(fmt.format0(args,i)); i += fmt.argCount(); }
		}
		return b.toString();
	}
	
	@Override
	public String toString() {
		var b = new StringBuilder();
		for(var fmt : data) b.append(fmt.toString());
		return b.toString();
	}
}
