package ring.formatting;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("TODO deduplicate")
class EnumUtil {
	static <T extends Enum<T>> int enumFlagsToInt(List<T> l) {
		int ret = 0;
		for(var e : l) {
			var tmp = 1<<e.ordinal();
			if(tmp == 0) throw new IllegalArgumentException("too many enum constants");
			ret |= tmp;
		}
		return ret;
	}
	
	static <T extends Enum<T>> List<T> intToEnumFlags(int value, Class<T> cls) {
		var l = new ArrayList<T>();
		for(var e : cls.getEnumConstants()) {
			if((value&(1<<e.ordinal())) != 0) l.add(e);
		}
		return l;
	}
}
