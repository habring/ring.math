package ring.formatting;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.List;

@SuppressWarnings("TODO replace by real ErrorMessages class")
final class ErrorMessages {
	public final String unreachable = "should never happen";
	
	public void checkArgTypes(List<Class<?>> argTypes, Object[] args) {
		checkArgCount(argTypes.size(),args.length);
		for(int i = 0; i < args.length; i++) {
			if(!argTypes.get(i).isInstance(args[i])) throw new IllegalArgumentException("argument "+i+" is expected to be of type "+argTypes.get(i));
		}
	}
	
	public int checkArgCount(int expected, int got) {
		if(expected == got) return got;
		throw new IllegalArgumentException("invalid number of arguments (expected "+expected+", got "+got+")");
	}
}
