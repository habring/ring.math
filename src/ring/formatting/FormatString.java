package ring.formatting;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.ArrayList;
import java.util.function.Supplier;

public abstract class FormatString {
	static final ErrorMessages errors = new ErrorMessages();
	public static final int noArgIndex = -1;
	public final int argIndex;
	
	FormatString(int argIndex) {
		this.argIndex = argIndex;
		if(argIndex < noArgIndex) throw new IllegalArgumentException("argIndex < 0");
	}
	
	public abstract int argCount();
	
	public final String format(Object...args) { return format(args,0); }
	
	protected abstract String format0(Object[] args, int offset);
	
	public static FormatString parse(String fmt) {
		class Input implements Supplier<Character> {
			int i = -1;
			@Override public Character get() { i++; return i < fmt.length() ? fmt.charAt(i) : null; }
		}
		var input = new Input();
		var l = new ArrayList<FormatString>();
		var b = new StringBuilder();
		while(true) {
			var la = input.get();
			if(b.length() > 0 && (la == null || la == '%')) {
				l.add(new ConstantFormatString(b.toString())); b.delete(0,b.length());
			}
			if(la == null) break;
			else if(la == '%') { input.i--; l.add(FormatSpecifier.parse(input)); input.i--; }
			else b.append(la);
		}
		switch(l.size()) {
		case 0: return ConstantFormatString.empty;
		case 1: return l.get(0);
		default: return new MergedFormatString(l);
		}
	}
	
	@Override public final int hashCode() { return toString().hashCode(); }
	
	@Override public final boolean equals(Object obj) { return obj != null && getClass() == obj.getClass() && toString().equals(obj.toString()); }
	
	@Override public abstract String toString();
}
