package ring.formatting;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.Objects;

public final class ConstantFormatString extends FormatString {
	public static final ConstantFormatString empty = new ConstantFormatString("");
	private final String str;
	
	public ConstantFormatString(String str) { super(noArgIndex); this.str = Objects.requireNonNull(str); }
	
	@Override public int argCount() { return 0; }
	
	@Override protected String format0(Object[] args, int offset) { return str; }
	
	@Override public String toString() { return str; }
}
