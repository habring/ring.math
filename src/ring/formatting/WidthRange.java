package ring.formatting;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

public final class WidthRange {
	public final int min, max;
	
	public WidthRange(int value) { this(value,value); }
	
	public WidthRange(int min, int max) {
		if(min == -1) min = 0;
		if(max == -1) max = Integer.MAX_VALUE;
		this.min = min; this.max = max;
		if(min < 0 || min > max) throw new IllegalArgumentException();
	}
	
	WidthRange add(WidthRange r) {
		long a = (long)min + r.min, b = (long)max + r.max;
		int x = (int)Math.min(a,Integer.MAX_VALUE), y = (int)Math.min(b,Integer.MAX_VALUE);
		return new WidthRange(x,y);
	}
	
	public int toRange(int value) { return value < min ? min : value > max ? max : value; }
	
	@Override public int hashCode() { return min*31 + max; }
	
	@Override public boolean equals(Object obj) { return obj instanceof WidthRange l && min == l.min && max == l.max; }
	
	@Override public String toString() { var b = new StringBuilder(); appendTo(b); return b.toString();  }
	
	void appendTo(StringBuilder b) {
		if(min != 0) b.append(min);
		if(max != Integer.MAX_VALUE) b.append(':').append(max);
	}
}
