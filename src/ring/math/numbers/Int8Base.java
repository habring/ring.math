package ring.math.numbers;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

interface Int8Base<N extends Int8Base<N>> extends IntBase<N> {
	public static final int bitLength = 64;
	
	long rawValue();
	N createRaw(long value);
	
//	constants
	
	@Override default N zero() { return createRaw(0); }
	@Override default N one() { return createRaw(1); }
	@Override default N two() { return createRaw(2); }
	@Override default int bitLength() { return bitLength; }
	
//	bit test
	
	@Override
	default int numberOfLeadingZeros() {
		long x = rawValue();
		if(x == 0) return bitLength;
		int n = 0;
		if((x&0xffffffff00000000L) == 0) { n+=32; x<<=32; }
		if((x&0xffff000000000000L) == 0) { n+=16; x<<=16; }
		if((x&0xff00000000000000L) == 0) { n+=8; x<<=8; }
		if((x&0xf000000000000000L) == 0) { n+=4; x<<=4; }
		if((x&0xc000000000000000L) == 0) { n+=2; x<<=2; }
		if((x&0x8000000000000000L) == 0) n++;
		return n;
	}
	@Override
	default int numberOfTrailingZeros() {
		long x = rawValue();
		if(x == 0) return bitLength;
		int n = 0;
		if((x&0x00000000ffffffffL) == 0) { n+=32; x>>=32; }
		if((x&0x000000000000ffffL) == 0) { n+=16; x>>=16; }
		if((x&0x00000000000000ffL) == 0) { n+=8; x>>=8; }
		if((x&0x000000000000000fL) == 0) { n+=4; x>>=4; }
		if((x&0x0000000000000003L) == 0) { n+=2; x>>=2; }
		if((x&0x0000000000000001L) == 0) n++;
		return n;
	}
	@Override
	default int bitCount() {
		long x = rawValue(); x -= (x>>>1) & 0x5555555555555555L;
		x = (x&0x3333333333333333L) + ((x>>>2) & 0x3333333333333333L);
		x = (x + (x>>>4)) & 0x0f0f0f0f0f0f0f0fL;
		x+=x>>>8; x+=x>>>16; x+=x>>>32; return (int)(x&0x7f);
	}
	@Override default boolean bit(int i) { return (rawValue()&(1L<<i)) != 0; }
	@Override default N setBit(int i, boolean b) { return createRaw(b ? rawValue()|(1L<<i) : rawValue()&(~(1L<<i))); }
	@Override
	default int indexOfBit(int n) {
		if(n < 0) throw new IllegalArgumentException();
		long v = rawValue();
		i4 h = new i4((int)(v>>>i4.bitLength)), l = new i4((int)(v&0xffffffff));
		var x = l.bitCount();
		if(n < x) return l.indexOfBit(n);
		n-=x; return i4.bitLength + h.indexOfBit(n);
	}
	
//	bitwise operations
	
	@Override default N and(N x) { return createRaw(rawValue() & x.rawValue()); }
	@Override default N or(N x) { return createRaw(rawValue() | x.rawValue()); }
	@Override default N xor(N x) { return createRaw(rawValue() ^ x.rawValue()); }
	@Override default N not() { return createRaw(~rawValue()); }
	@Override default N notWithoutSign() { return createRaw(((~rawValue())&maxValue().rawValue()) | (rawValue()&minValue().rawValue())); }
	@Override default N shl(int amount) { return createRaw(rawValue() << amount); }
	
//	Object overrides
	
	default int hashCode0() { long v = rawValue(); return (int)(v ^ (v>>32)); }
	
	
	
	static interface I8Base<N extends I8Base<N>> extends Int8Base<N>, IBase<N> {
//		constants
		
		@Override default N minValue() { return createRaw(0x8000000000000000L); }
		@Override default N maxValue() { return createRaw(0x7fffffffffffffffL); }
		
//		basic arithmetic
		
		@Override default N mod(N n) { return createRaw(rawValue() % n.rawValue()); }
		
	//	bitwise operations
		
		@Override default N shr(int amount) { return createRaw(rawValue() >> amount); }
		
	//	Object overrides
		
		@Override default int compareTo(N n) { return rawValue() < n.rawValue() ? -1 : rawValue() > n.rawValue() ? 1 : 0; }
	}
	
	static interface U8Base<N extends U8Base<N>> extends Int8Base<N>, UBase<N> {
//		constants
		
		@Override default N maxValue() { return createRaw(0xffffffffffffffffL); }
		
//		basic arithmetic
		
		@Override default N mod(N n) { return createRaw(Long.remainderUnsigned(rawValue(),n.rawValue())); }
		
	//	bitwise operations
		
		@Override default N shr(int amount) { return createRaw(rawValue() >>> amount); }
		
	//	Object overrides
		
		@Override default int compareTo(N n) { return Long.compareUnsigned(rawValue(),n.rawValue()); }
	}
	
	static interface S8Base<N extends S8Base<N>> extends Int8Base<N>, SBase<N> {
//		basic arithmetic
		
		@Override default N add(N n) { return createRaw(Math.addExact(rawValue(),n.rawValue())); }
		@Override default N sub(N n) { return createRaw(Math.subtractExact(rawValue(),n.rawValue())); }
		@Override default N mul(N n) { return createRaw(Math.multiplyExact(rawValue(),n.rawValue())); }
	}
	
	static interface W8Base<N extends W8Base<N>> extends Int8Base<N>, WBase<N> {
//		basic arithmetic
		
		@Override default N add(N n) { return createRaw(rawValue() + n.rawValue()); }
		@Override default N sub(N n) { return createRaw(rawValue() - n.rawValue()); }
		@Override default N mul(N n) { return createRaw(rawValue() * n.rawValue()); }
	}
}
