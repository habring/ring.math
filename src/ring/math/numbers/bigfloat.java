package ring.math.numbers;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import static ring.math.numbers.bigint.errors;

import java.util.Set;
import java.util.function.Supplier;

import ring.formatting.FormatSpecifier;
import ring.util.SupplierFactory;

public final class bigfloat implements FloatBase<bigfloat> {
	private static final bigfloat zero = new bigfloat(0), one = new bigfloat(1), two = new bigfloat(2);
	static final bigfloat negInfinity = new bigfloat(-1,Kind.irregular), posInfinity = new bigfloat(1,Kind.irregular);
	static final bigfloat NaN = new bigfloat(0,Kind.irregular);
//	when a period is detected, at max MAX_DIV_P bits are generated
	private static final int defaultDivP = 2048, defaultLogP = 64;
	private static enum Kind { exact, rounded, irregular }
	
//	whole number == val * 2^exp == val << exp
	private final bigint val; //mantissa
	private final int exp; //exponent
	private final Kind t; //kind
	private final int p; //max precision
	
	private bigfloat(bigint val, int exp, Kind t, int p) {
		if(p < 0) throw new Error(errors.unreachable);
		if(t == Kind.irregular) { val = new bigint(val.signum()); exp = 0; }
		int sh = val.bitLength() - p;
		if(sh > 0 && p != 0 && t != Kind.irregular) { //truncate
			val = val.shr(sh-1).roundUlp();
			exp = Math.toIntExact(exp+sh);
			t = Kind.rounded;
		}
		int tz = val.numberOfTrailingZeros();
		exp += tz; val = val.shr(tz);
		if(val.signum() == 0) exp = 0;
		this.val = val; this.exp = exp; this.t = t; this.p = p;
	}
	private bigfloat(int signum, Kind t) { this(new bigint(signum),0,t,0); }
	private bigfloat(bigfloat f) { this(f.val,f.exp,f.t,f.p); }
	
	public bigfloat(long value) { this(new bigint(value),0); }
	public bigfloat(double value) { this(parse(value)); }
	public bigfloat(bigint value) { this(value,0); }
	public bigfloat(bigint mantissa, int exponent) { this(mantissa,exponent,true); }
	public bigfloat(bigint mantissa, int exponent, boolean isExact) {
		this(mantissa,exponent,isExact?Kind.exact:Kind.rounded,0);
	}
	
	private static int p(bigfloat a, bigfloat b) { return p(a.p,b.p); }
	private static int p(int p1, int p2) { var x = Math.min(p1,p2); return x != 0 ? x : p1 != 0 ? p1 : p2; }
	
	
	
//	constants
	
	@Override public bigfloat zero() { return zero; }
	@Override public bigfloat one() { return one; }
	@Override public bigfloat two() { return two; }
	@Override public bigfloat minValue() { return null; }
	@Override public bigfloat maxValue() { return null; }
	@Override public bigfloat negInfinity() { return negInfinity; }
	@Override public bigfloat posInfinity() { return posInfinity; }
	@Override public bigfloat ulp() { return isIrregular() ? NaN() : new bigfloat(new bigint(1),exp,t,p); }
	@Override public bigfloat NaN() { return NaN; }
	/**The length in bits. Consistency with {@link #maxPrecision()} is guaranteed.*/
	@Override public int bitLength() { return val.bitLength(); }
	
	
	
//	conversion
	
	/**Indicates whether this bigfloat is a result of an exact computation or was rounded at some point in time.*/
	public boolean isExact() { return t == Kind.exact; }
	public bigfloat setExact(boolean exact) {
		if(t == Kind.irregular || exact == isExact()) return this;
		return new bigfloat(val,exp,exact?Kind.exact:Kind.rounded,p);
	}
	/**The result of any computation on this bigfloat can have at most this number of bits. Zero means infinite.*/
	public int maxPrecision() { return p; }
	public bigfloat setMaxPrecision(int precision) {
		if(precision < 0) throw new IllegalArgumentException();
		if(precision == Integer.MAX_VALUE) precision = 0;
		return precision == p ? this : new bigfloat(val,exp,t,precision);
	}
	public bigint mantissa() { return checkRegular().val; }
	public int exponent() { return checkRegular().exp; }
	@Override public boolean isNaN() { return isIrregular() && val.signum() == 0; }
	private boolean isInfinity() { return isIrregular() && val.signum() != 0; }
	@Override public boolean isIrregular() { return t == Kind.irregular; }
	private bigfloat checkRegular() {
		if(!isIrregular()) return this;
		throw new UnsupportedOperationException();
	}
	@Override public Set<Class<?>> containedClasses() { return ContainedClasses._bigfloat; }
	
	/**https://en.wikipedia.org/wiki/Double-precision_floating-point_format*/
	private static bigfloat parse(double d) {
		long raw = Double.doubleToRawLongBits(d);
		var sig = raw >= 0 ? 1 : -1; int expBits = (int)((raw>>>52)&0x7ff), exp; long frac = raw&0xfffffffffffffL;
		if(expBits == 0) { //signed zero or subnormal
			if(frac == 0) return zero;
			exp = -1022;
		}
		else if(expBits == 0x7ff) { //inf and nan
			return frac != 0 ? NaN : sig >= 0 ? posInfinity : negInfinity;
		}
		else { //normal
			exp = expBits - 1023;
			frac |= 1L<<52;
		}
		return new bigfloat(new bigint(frac).setSignum(sig),exp-52,Kind.exact,0);
	}
	f8 roundToF8() {
		if(p == 0 || p > 54) return setMaxPrecision(54).roundToF8();
		long sig = signum() >= 0 ? 0 : 1L<<63;
		int expBits; long frac;
		if(isIrregular()) { expBits = 0x7ff; frac = signum() == 0 ? 1 : 0; }
		else if(signum() == 0) { expBits = 0; frac = 0; }
		else {
			long rawMag = val.abs().magToUi64(); int lz = 54-val.bitLength();
			rawMag <<= lz;
			if((rawMag&~0xfffffffffffffL) != 1L<<52 || lz < 0) throw new Error(errors.unreachable);
			int exp = this.exp + (52-lz);
			expBits = exp + 1023;
			if(expBits >= 0x7ff) { expBits = 0x7ff; frac = 0; } //overflow -> inf
			else if(expBits <= 0) { //subnormal
				expBits = 0; frac = rawMag >>> -(exp+1022);
				if((frac&~0xfffffffffffffL) != 0) throw new Error(errors.unreachable);
			}
			else frac = rawMag & 0xfffffffffffffL; //normal
		}
		long raw = sig | ((long)expBits<<52) | frac;
		return new f8(Double.longBitsToDouble(raw));
	}
	
	
	
//	basic arithmetic
	
	@Override
	public bigfloat add(bigfloat n) {
		if(exp < n.exp) return n.add(this);
		if(isIrregular()) return isNaN() || signum() != n.signum() ? NaN() : this;
		else if(n.isIrregular()) return n.add(this);
		int sh = exp - n.exp, p = p(this,n);
		bigint a = val, b = n.val;
//		result = (a<<sh) + b * 2^x.exp
		int al = a.bitLength();
		if(p == 0 || al+sh <= p) {
			var t = isExact() && n.isExact() ? Kind.exact : Kind.rounded;
			return new bigfloat(a.shl(sh).add(b),n.exp,t,p);
		}
		p++; a = a.shl(p-al); b = b.shr(sh-(p-al));
		var exp = Math.toIntExact(this.exp-(p-al));
		return new bigfloat(a.add(b),exp,Kind.rounded,p);
	}
	@Override public bigfloat sub(bigfloat x) { return add(x.neg()); }
	@Override
	public bigfloat mul(bigfloat n) {
		var t = isIrregular() ? Kind.irregular : isExact() ? n.t : Kind.rounded;
		return new bigfloat(val.mul(n.val),Math.toIntExact(exp+(long)n.exp),t,p(this,n));
	}
	@Override
	public bigfloat div(bigfloat n) {
		if(n.signum() == 0) return NaN();
		if(isNaN() || n.isNaN() || (isInfinity() && n.isInfinity())) return NaN();
		if(isInfinity()) return n.signum() > 0 ? this : neg();
		if(signum() == 0) return this;
		if(n.isInfinity()) return new bigfloat(new bigint(0),0,Kind.rounded,p);
		class L { int value; }
		var exp = new L(); exp.value = Math.toIntExact(this.exp - (long)n.exp);
		bigint a = val, b = n.val; var p = p(this,n);
		var qlen = p != 0 ? p : Math.max(a.bitLength()+b.bitLength(),defaultDivP);
		var q = bigint.divmod(a,b,(q0,r0) -> {
			var tmp = q0.bitLength() <= qlen;
			if(tmp) exp.value--;
			return tmp;
		});
		var t = isExact() && n.isExact() && q[1].signum() == 0 ? Kind.exact : Kind.rounded;
		return new bigfloat(q[0],exp.value,t,p);
	}
	@Override public bigfloat neg() { return new bigfloat(val.neg(),exp,t,p); }
	@Override public int signum() { return val.signum(); }
	@Override public bigfloat setSignum(int signum) { return new bigfloat(val.setSignum(signum),exp,t,p); }
	
	
	
//	round
	
	@Override public bigfloat floor() { return signum() >= 0 || exp >= 0 ? cut() : cut().sub(one); }
	@Override public bigfloat ceil() { return signum() < 0 || exp >= 0 ? cut() : cut().add(one); }
	@Override public bigfloat round() { return exp >= 0 || isIrregular() ? this : new bigfloat(roundToBigInt(),0,Kind.rounded,p); }
	bigint roundToBigInt() {
		if(isIrregular()) return null;
		else if(exp >= 0) return val.shl(exp);
		else return val.shl(exp+1).roundUlp();
	}
	@Override public bigfloat cut() { return isIrregular() || exp >= 0 ? this : new bigfloat(val.shl(exp),0,Kind.rounded,p); }
	
	
	
//	complex operations
	
	@Override
	/**https://de.wikipedia.org/wiki/Logarithmus#Berechnung_einzelner_Bin%C3%A4rziffern*/
	public bigfloat log2() {
		if(signum() <= 0) return NaN();
		if(equals(posInfinity)) return this;
		var shr = exp + val.bitLength()-2;
		var x = shr0(shr);
		if(x.compareTo(one) < 0 || x.compareTo(two) >= 0 || isIrregular()) throw new Error(errors.unreachable);
//		1 <= x < 2
		var p = this.p == 0 ? defaultLogP : this.p-new bigint(shr).bitLength()+1;
//		this is the minimal precision you need for x in order to compute p bits
		x = x.setMaxPrecision(p*p+1);
		var ret = new bigint(0);
		int bits;
		for(bits = 0; x.signum() != 0 && bits < p; bits++) {
			if(x.compareTo(one) < 0 || x.compareTo(two) >= 0 || isIrregular()) throw new Error(errors.unreachable);
			x = x.mul(x);
			ret = ret.shl(1);
			var bit = x.compareTo(two) >= 0;
			if(bit) x = x.div(two);
			ret = ret.setBit(0,bit);
		}
		var frac = new bigfloat(ret,-bits,x.signum()==0);
		return new bigfloat(shr).add(frac).setMaxPrecision(this.p);
	}
	private bigfloat shl0(int amount) { checkRegular(); return new bigfloat(val,Math.toIntExact(exp+amount),t,p); }
	private bigfloat shr0(int amount) { checkRegular(); return amount == 0 ? this : abs().shl0(-amount); }
	
	
	
//	Object overrides
	
	@Override public int hashCode() { return val.hashCode() ^ exp ^ t.hashCode() ^ new i8(p).hashCode(); }
	@Override public boolean equals(Object obj) { return equals0(obj); }
	@Override
	public int compareTo(bigfloat n) {
		int sig = signum();
		if(sig != n.signum()) return Integer.compare(sig,n.signum());
		if(isIrregular()) {
			if(isNaN() || n.isNaN()) throw new ArithmeticException();
			return n.isIrregular() ? 0 : 1;
		}
		else if(n.isIrregular()) return -n.compareTo(this);
		var ar = toSameBitLength(this,n);
		int ret = Integer.compare(ar.exp1,ar.exp2);
		return ret == 0 ? ar.a.compareTo(ar.b) : sig > 0 ? ret : -ret;
	}
	private static class Arranged {
		final bigint a, b;
		final int exp1, exp2;
		Arranged(bigint a, int exp1, bigint b, int exp2) {
			this.a = a; this.exp1 = exp1; this.b = b; this.exp2 = exp2;
		}
		Arranged swap() { return new Arranged(b,exp2,a,exp1); }
	}
	/**prepare two bigfloats for a value-combining operation.*/
	private static Arranged toSameBitLength(bigfloat a, bigfloat b) {
		int al = a.val.bitLength(), bl = b.val.bitLength();
		if(al < bl) return toSameBitLength(b,a).swap();
		else if(al == bl) return new Arranged(a.val,a.exp,b.val,b.exp);
		var tmp = b.val.shl(al-bl);
		if(al != tmp.bitLength()) throw new Error(errors.unreachable);
		return new Arranged(a.val,a.exp,tmp,Math.toIntExact(b.exp-(al-bl)));
	}
	public static bigfloat parse(String str) {
		var tmp = SupplierFactory.create(str);
		var ret = BigFloatSerializer.parse(tmp);
		if(tmp.get() == null) return ret;
		throw new IllegalArgumentException("end of input expected");
	}
	public static bigfloat parse(Supplier<Character> input) { return BigFloatSerializer.parse(input); }
	@Override public String toString() { return toString(FormatSpecifier.string); }
	@Override public String toString(FormatSpecifier fmt) { return BigFloatSerializer.toString(this,fmt); }
}
