package ring.math.numbers;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.Set;

import ring.formatting.FormatSpecifier;
import ring.math.numbers.Int8Base.I8Base;
import ring.math.numbers.Int8Base.S8Base;

public final class i8s implements I8Base<i8s>, S8Base<i8s> {
	private final long value;
	
	public i8s(long value) { this.value = value; }
	
	@Override public long rawValue() { return value; }
	
	@Override public i8s createRaw(long value) { return new i8s(value); }
	
//	conversion
	
	@Override public Set<Class<?>> containedClasses() { return ContainedClasses._i8s; }
	
//	basic arithmetic
	
	@Override
	public i8s div(i8s n) {
		long x = rawValue(), y = n.rawValue();
		if(x % y == 0) return createRaw(x / y);
		throw new ArithmeticException(this+" % "+n+" != 0");
	}
	
//	Object overrides
	
	@Override public int hashCode() { return hashCode0(); }
	
	@Override public boolean equals(Object obj) { return equals0(obj); }
	
	@Override public String toString() { return toString(FormatSpecifier.string); }
}
