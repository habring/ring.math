package ring.math.numbers;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import static ring.math.numbers.NumberConverters.errors;

import java.util.*;
import java.util.function.Supplier;

import ring.formatting.FormatSpecifier;
import ring.formatting.FormatSpecifier.Flag;

/**(+|-|)(0b|0x|0r[0-9]+r|)[0-9a-zA-Z]+(\.[0-9a-zA-Z]+|)((_|)(e|E)(-|)[0-9]+|)<br>
 * (+|-|)(NaN|infinity)<br>
 * simplified: [+\-\._0-9a-zA-Z]+*/
class BigFloatSerializer {
	/**whole number == (isPositive?1:-1) * digits[0]digits[1]...digits[n] * radix^exp*/
	private static class Digits {
		final int[] digits; final int radix, exp;
		final boolean isPositive;
		
		Digits(int sign) {
			this.radix = -1; this.isPositive = sign >= 0;
			this.digits = new int[] {sign}; this.exp = 0;
		}
		
		Digits(int radix, boolean isPositive, int[] digits, int exp) {
			this.radix = num.checkRadix(radix);
			this.isPositive = isPositive;
			this.digits = Objects.requireNonNull(digits);
			this.exp = exp;
		}
		
		int magDigits() { return Math.max(0,digits.length+exp); }
		
		int fracDigits() { return Math.max(0,-exp); }
		
		char digitToChar(int i) {
			var tmp = i < digits.length ? digits[i] : 0;
			if(tmp < 0 || tmp >= num.maxRadix) throw new Error(errors.unreachable);
			return (char)(tmp < 10 ? '0'+tmp : 'a'+tmp-10);
		}
		
		static int charToDigit(int radix, char c) {
			int ret = charToDigitUnsafe(c);
			if(0 <= ret && ret < radix) return ret;
			throw new IllegalArgumentException("invalid digit "+c+" in radix "+radix);
		}
		
		static int charToDigitUnsafe(char c) {
			int ret = -1;
			if('0' <= c && c <= '9') ret = c-'0';
			else if('a' <= c && c <= 'z') ret = c-'a'+10;
			else if('A' <= c && c <= 'Z') ret = c-'A'+10;
			return ret;
		}
	}
	
	private static final int defaultMaxNumDigits = 10;
	private static final int maxRadixWithoutUsc = 14;
	static final double[] radixLog2;
	private static final bigfloat[] i2f;
	
	static {
		radixLog2 = new double[num.maxRadix+1];
		for(int radix = 0; radix <= num.maxRadix; radix++) {
			radixLog2[radix] = new bigfloat(radix).setMaxPrecision(16).log2().convertTo(f8.class).rawValue();
		}
		i2f = new bigfloat[num.maxRadix+1];
		for(int i = 0; i < i2f.length; i++) i2f[i] = new bigfloat(i);
	}
	
	static bigfloat parse(Supplier<Character> input) {
		var d = parse0(input);
		if(d.radix < 0) return d.digits[0] == 0 ? bigfloat.NaN : d.isPositive ? bigfloat.posInfinity : bigfloat.negInfinity;
		var f = i2f[0];
		for(var digit : d.digits) {
			f = f.mul(i2f[d.radix]).add(i2f[digit]);
		}
		var numBits = digitsToNumBits(d.radix,d.digits.length);
		var radixPowExp = i2f[d.radix].setMaxPrecision(numBits).pow(new bigfloat(d.exp).abs());
		f = d.exp >= 0 ? f.mul(radixPowExp) : f.div(radixPowExp);
		return f.setMaxPrecision(0).setSignum(d.isPositive?1:-1);
	}
	
	static String toString(bigfloat f, FormatSpecifier fmt) {
		Objects.requireNonNull(fmt);
		if(f.isIrregular()) {
			var tmp = f.isNaN() ? "NaN" : f.signum() > 0 ? "infinity" : "-infinity";
			return pad(padSign(tmp,f.signum()>=0,fmt.flags),fmt.totalWidth.min,!fmt.flags.contains(Flag.leftJustify),' ');
		}
		int len = fmt.totalWidth.max;
		if(len < Integer.MAX_VALUE) len++;
		else len = defaultMaxNumDigits*2 + 1;
		return toString0(toRadix(f,fmt.radix,len),fmt);
	}
	
	private static Digits toRadix(bigfloat f, int radix, int maxNumDigits) {
		num.checkRadix(radix);
		if(maxNumDigits <= 0) throw new Error(errors.unreachable);
		int signum = f.signum(); f = f.abs();
		int maxNumBits = digitsToNumBits(radix,maxNumDigits);
//		reduce exponent
		bigfloat radixF = new bigfloat(radix).setMaxPrecision(maxNumBits), radixPowM1 = null;
		int exp = 0;
		while(f.exponent() < -f.mantissa().bitLength()+1) { f = f.mul(radixF); exp--; }
		while(f.exponent() > radixLog2[radix]) {
			if(radixPowM1 == null) radixPowM1 = new bigfloat(1).div(radixF);
			f = f.mul(radixPowM1); exp++;
		}
//		convert integer part
		var magF = f.cut(); var digits = magF.roundToBigInt().toRadix(radix);
//		convert fraction part
		var fracF = f.sub(magF); int fracLen = Math.max(0,maxNumDigits-digits.length);
		digits = Arrays.copyOf(digits,digits.length+fracLen);
		for(int i = digits.length-fracLen; i < digits.length; i++) {
			if(fracF.signum() == 0) break;
			fracF = fracF.mul(radixF); var digitF = fracF.cut();
			digits[i] = digitF.convertTo(i4.class).rawValue();
			if(digits[i] < 0 || digits[i] >= radix) throw new Error(errors.unreachable);
			fracF = fracF.sub(digitF);
		}
		exp -= fracLen; int tz = 0;
		for(int i = digits.length-1; i >= 0 && digits[i] == 0; i--) tz++;
		if(tz > 0) { digits = Arrays.copyOf(digits,digits.length-tz); exp+=tz; }
		return new Digits(radix,signum>=0,digits,exp);
	}
	
	private static Digits parse0(Supplier<Character> input) {
		var digits = new ArrayList<i4>(); int radix = 10, exp = 0; var isPositive = true;
		var la = next(input);
		if(la == '+') la = next(input);
		else if(la == '-') { isPositive = false; la = next(input); }
		switch(la) {
		case '0':
			var tmp = input.get();
			if(tmp == null) return new Digits(radix,true,new int[0],exp);
			if('0' <= tmp && tmp <= '9') { digits.add(new i4(0)); la = tmp; }
			else {
				switch(tmp) {
				case 'b': radix = 2; la = next(input); break;
				case 'x': radix = 16; la = next(input); break;
				case 'r':
					radix = 0;
					while((la = next(input)) != 'r') {
						radix = radix*10 + Digits.charToDigit(10,la);
					}
					num.checkRadix(radix); la = next(input);
					break;
				default: throw new IllegalArgumentException("invalid radix prefix 0"+tmp);
				}
			}
			break;
		case 'N':
			nextStr(input,"NaN");
			return new Digits(0);
		case 'i':
			nextStr(input,"infinity");
			return new Digits(isPositive?1:-1);
		default:
			if('0' <= la && la <= '9') break;
			throw new IllegalArgumentException("invalid start of number "+la);
		}
		int isFrac = 0;
		while(true) {
			int digit = Digits.charToDigitUnsafe(la);
			if(digit < 0 || digit >= radix) break;
			digits.add(new i4(digit)); exp -= isFrac;
			var tmp = input.get();
			if(tmp == null) { la = 0; break; }
			if(tmp == '.') { isFrac = 1; la = next(input); continue; }
			la = tmp;
		}
		if(isFrac != 0 && exp == 0) throw new IllegalArgumentException("empty fraction");
		boolean hasExp = false;
		if(la == '_') { la = next(input); hasExp = true; }
		if(la == 'e' || la == 'E') {
			int exp0 = 0, exp0Sign = 1, exp0Len = 0;
			la = next(input);
			if(la == '-') { exp0Sign = -1; la = next(input); }
			while(true) {
				int digit = Digits.charToDigitUnsafe(la);
				if(digit < 0 || digit >= 10) break;
				exp0 = exp0*10 + digit; exp0Len++;
				var tmp = input.get();
				if(tmp == null) break;
				la = tmp;
			}
			if(exp0Len == 0) throw new IllegalArgumentException("empty exponent");
			exp += exp0 * exp0Sign;
		}
		else if(hasExp) throw new IllegalArgumentException("no exponent");
		return new Digits(radix,isPositive,toArray(digits),exp);
	}
	
	private static String toString0(Digits d, FormatSpecifier fmt) {
		var prefix = padSign(d.isPositive ? "" : "-",d.isPositive,fmt.flags) + radixToString(d.radix);
		if(fmt.flags.contains(Flag.noScientificForm)) return toNormalForm(prefix,d,fmt);
		if(fmt.flags.contains(Flag.scientificForm)) return toScientificForm(prefix,d,fmt);
		int magLen = d.magDigits(), fracLen = d.fracDigits();
		int maxMag = maxDigits(fmt.magWidth.max), maxFrac = maxDigits(fmt.fracWidth.max);
		if(magLen <= maxMag && fracLen <= maxFrac) return toNormalForm(prefix,d,fmt);
		else if(fracLen == 0 && magLen < Math.min(2*defaultMaxNumDigits,fmt.totalWidth.max)) return toNormalForm(prefix,d,fmt);
		else return toScientificForm(prefix,d,fmt);
	}
	
	private static String toNormalForm(String prefix, Digits d, FormatSpecifier fmt) {
		int magLen = d.magDigits(), fracLen = fmt.fracWidth.toRange(d.fracDigits());
		var b = new StringBuilder(prefix);
		if(magLen > 0) {
			for(int i = 0; i < magLen; i++) b.append(d.digitToChar(i));
			if(fracLen > 0) b.append('.');
		}
		else {
			b.append('0');
			int pad = -(d.digits.length+d.exp);
			if(pad > 0) b.append('.');
			for(int i = 0; i < pad; i++) b.append('0');
		}
		for(int i = 0; i < fracLen; i++) b.append(d.digitToChar(magLen+i));
		return pad(b.toString(),fmt.totalWidth.min,true,fmt.flags);
	}
	
	private static String toScientificForm(String prefix, Digits d, FormatSpecifier fmt) {
		int exp = d.exp + d.digits.length-1; var suffix = expToString(d.radix,exp);
		var b = new StringBuilder(prefix).append(d.digitToChar(0));
		int fracLen = fmt.fracWidth.toRange(d.digits.length-1);
		if(fracLen > 0) {
			b.append(".");
			for(int i = 1; i <= fracLen; i++) b.append(d.digitToChar(i));
		}
		return b.append(suffix).toString();
	}
	
	private static int maxDigits(int max) { return max == Integer.MAX_VALUE ? defaultMaxNumDigits : max; }
	
	private static String radixToString(int radix) {
		switch(radix) {
		case 2: return "0b";
		case 10: return "";
		case 16: return "0x";
		default: return "0r" + radix + "r";
		}
	}
	
	private static String expToString(int radix, int exp) {
		if(exp == 0) return "";
		var b = new StringBuilder();
		if(radix > maxRadixWithoutUsc) b.append("_");
		return b.append("e").append(exp).toString();
	}
	
	private static String padSign(String str, boolean hasHiddenPSign, Set<Flag> flags) {
		var prefix = !hasHiddenPSign ? "" : flags.contains(Flag.showSign) ? "+" : flags.contains(Flag.showSpaceSign) ? " " : "";
		return prefix+str;
	}
	
	private static String pad(String str, int length, boolean before, Set<Flag> flags) {
		return pad(str,length,before,flags.contains(Flag.padZero)?'0':' ');
	}
	
	private static String pad(String str, int length, boolean before, char padChar) {
		var b = new StringBuilder();
		for(int num = length-str.length(); num > 0; num--) b.append(padChar);
		return (before ? b.insert(0,str) : b.append(str)).toString();
	}
	
	private static void nextStr(Supplier<Character> input, String expected) {
		for(int i = 1; i < expected.length(); i++) {
			var la = next(input);
			if(la != expected.charAt(i)) throw new IllegalArgumentException("string constant "+expected+" expected");
		}
	}
	
	private static char next(Supplier<Character> input) {
		var ret = input.get();
		if(ret != null) return ret;
		throw new IllegalArgumentException("unexpected end of input");
	}
	
	private static int digitsToNumBits(int radix, int numDigits) {
//		find smallest numBits: 2^numBits >= radix^numDigits
//		numBits >= log2(radix^numDigits)
		return new f8(radixLog2[radix] * numDigits).ceil().convertTo(i4.class).rawValue();
	}
	
	private static int[] toArray(List<i4> l) {
		var ret = new int[l.size()];
		for(int i = 0; i < l.size(); i++) ret[i] = l.get(i).rawValue();
		return ret;
	}
}
