package ring.math.numbers;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

interface Int1Base<N extends Int1Base<N>> extends IntBase<N> {
	public static final int bitLength = 8;
	
	byte rawValue();
	N createRaw(byte value);
	default N createRaw(int value) { return createRaw((byte)(value&0xff)); }
	
//	constants
	
	@Override default N zero() { return createRaw(0); }
	@Override default N one() { return createRaw(1); }
	@Override default N two() { return createRaw(2); }
	@Override default int bitLength() { return bitLength; }
	
//	bit test
	
	@Override
	default int numberOfLeadingZeros() {
		byte x = rawValue();
		if(x == 0) return 8;
		int n = 0;
		if((x&0xf0) == 0) { n+=4; x<<=4; }
		if((x&0xc0) == 0) { n+=2; x<<=2; }
		if((x&0x80) == 0) n++;
		return n;
	}
	@Override
	default int numberOfTrailingZeros() {
		byte x = rawValue();
		if(x == 0) return bitLength;
		int n = 0;
		if((x&0x0f) == 0) { n+=4; x>>=4; }
		if((x&0x03) == 0) { n+=2; x>>=2; }
		if((x&0x01) == 0) n++;
		return n;
	}
	@Override
	default int bitCount() {
		byte x = rawValue(); x -= (x>>>1) & 0x55;
		x = (byte)((x&0x33) + ((x>>>2) & 0x33));
		x = (byte)((x + (x>>>4)) & 0x0f);
		return x&0x0f;
	}
	@Override default boolean bit(int i) { return (rawValue()&(1<<i)) != 0; }
	@Override default N setBit(int i, boolean b) { return createRaw(b ? rawValue()|(1<<i) : rawValue()&(~(1<<i))); }
	@Override default int indexOfBit(int n) { return Helper.indexOfBit(this,n); }
	
//	bitwise operations
	
	@Override default N and(N x) { return createRaw(rawValue() & x.rawValue()); }
	@Override default N or(N x) { return createRaw(rawValue() | x.rawValue()); }
	@Override default N xor(N x) { return createRaw(rawValue() ^ x.rawValue()); }
	@Override default N not() { return createRaw(~rawValue()); }
	@Override default N notWithoutSign() { return createRaw(((~rawValue())&maxValue().rawValue()) | (rawValue()&minValue().rawValue())); }
	@Override default N shl(int amount) { return createRaw(rawValue() << amount); }
	
//	Object overrides
	
	default int hashCode0() { return rawValue(); }
	
	
	
	static interface I1Base<N extends I1Base<N>> extends Int1Base<N>, IBase<N> {
//		constants
		
		@Override default N minValue() { return createRaw(0x80); }
		@Override default N maxValue() { return createRaw(0x7f); }
		
//		basic arithmetic
		
		@Override default N mod(N n) { return createRaw(rawValue() % n.rawValue()); }
		
	//	bitwise operations
		
		@Override default N shr(int amount) { return createRaw(rawValue() >> amount); }
		
	//	Object overrides
		
		@Override default int compareTo(N n) { return rawValue() < n.rawValue() ? -1 : rawValue() > n.rawValue() ? 1 : 0; }
	}
	
	static interface U1Base<N extends U1Base<N>> extends Int1Base<N>, UBase<N> {
//		constants
		
		@Override default N maxValue() { return createRaw(0xff); }
		
//		basic arithmetic
		
		@Override default N mod(N n) { return createRaw(Integer.remainderUnsigned(rawValue()&0xff,n.rawValue()&0xff)); }
		
	//	bitwise operations
		
		@Override default N shr(int amount) { return createRaw(rawValue() >>> amount); }
		
	//	Object overrides
		
		@Override default int compareTo(N n) { return Byte.compareUnsigned(rawValue(),n.rawValue()); }
	}
	
	static interface S1Base<N extends S1Base<N>> extends Int1Base<N>, SBase<N> {
//		basic arithmetic
		
		@Override default N add(N n) { return createRaw(Math.addExact(rawValue(),n.rawValue())); }
		@Override default N sub(N n) { return createRaw(Math.subtractExact(rawValue(),n.rawValue())); }
		@Override default N mul(N n) { return createRaw(Math.multiplyExact(rawValue(),n.rawValue())); }
	}
	
	static interface W1Base<N extends W1Base<N>> extends Int1Base<N>, WBase<N> {
//		basic arithmetic
		
		@Override default N add(N n) { return createRaw(rawValue() + n.rawValue()); }
		@Override default N sub(N n) { return createRaw(rawValue() - n.rawValue()); }
		@Override default N mul(N n) { return createRaw(rawValue() * n.rawValue()); }
	}
}
