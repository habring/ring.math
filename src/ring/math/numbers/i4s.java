package ring.math.numbers;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.Set;
import java.util.function.Supplier;

import ring.formatting.FormatSpecifier;
import ring.math.numbers.Int4Base.I4Base;
import ring.math.numbers.Int4Base.S4Base;

public final class i4s implements I4Base<i4s>, S4Base<i4s> {
	private final int value;
	
	public i4s(int value) { this.value = value; }
	
	@Override public int rawValue() { return value; }
	
	@Override public i4s createRaw(int value) { return new i4s(value); }
	
//	conversion
	
	@Override public Set<Class<?>> containedClasses() { return ContainedClasses._i4s; }
	
//	basic arithmetic
	
	@Override
	public i4s div(i4s n) {
		int x = rawValue(), y = n.rawValue();
		if(x % y == 0) return createRaw(x / y);
		throw new ArithmeticException(this+" % "+n+" != 0");
	}
	
//	Object overrides
	
	@Override public int hashCode() { return hashCode0(); }
	
	@Override public boolean equals(Object obj) { return equals0(obj); }
	
	@Override public String toString() { return toString(FormatSpecifier.string); }
	
	public static i4s parseDecimal(Supplier<Character> input) {
		var ret = new i4s(0);
		while(true) {
			var la = input.get();
			if(la == null) break;
			if(!isDecimalDigit(la)) throw new IllegalArgumentException("char "+la+" is not a digit");
			ret = ret.mul(new i4s(10)).add(new i4s(la-'0'));
		}
		return ret;
	}
	
	public static boolean isDecimalDigit(Character c) { return '0' <= c && c <= '9'; }
}
