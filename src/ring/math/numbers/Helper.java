package ring.math.numbers;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.Comparator;
import java.util.function.BinaryOperator;

class Helper {
	private static final int[][] byteIndexOfBit;
	
	static {
		byteIndexOfBit = new int[256][8];
		for(int v = 0; v < 256; v++) {
			for(int n = 0; n < 8; n++) {
				byteIndexOfBit[v][n] = (int)slowIndexOfBit(new i8((byte)v),n);
			}
		}
	}
	
	static int indexOfBit(Int1Base<?> num, int n) {
		if(n < 0) throw new IllegalArgumentException();
		byte v = num.rawValue();
		if(n >= i1.bitLength) return v < 0 ? i1.bitLength+n-num.bitCount() : -1;
		return byteIndexOfBit[v&0xff][n];
	}
	
	private static <N extends num<N>> long slowIndexOfBit(N num, long n) {
		if(n < 0) throw new IllegalArgumentException();
		long ret = 0;
		while(true) {
			if(num.bit(0)) {
				if(n == 0) return ret;
				n--;
			}
			if(num.signum() == 0) return -1;
			num = num.shr(1); ret++;
		}
	}
	
	static <N extends num<N>> N nroot(N x, N n) {
		N one = x.one();
		if(n.compareTo(one) <= 0) throw new ArithmeticException("n <= 1");
		if(x.compareTo(one) <= 0) {
			if(x.compareTo(one) == 0) return one;
			throw new ArithmeticException("v <= 0");
		}
		return binarySearch(one,x,x,(e,t)->e.pow(n).compareTo(t));
	}
	
	/**https://en.wikipedia.org/wiki/Exponentiation_by_squaring*/
	static <N extends num<N>,I extends num<I>> N pow(N x, BinaryOperator<N> mul, I n) {
		if(!n.isInt()) {
			var tmp = n.convertToLossless(bigint.class);
			if(tmp != null) return pow(x,mul,tmp);
			throw new IllegalArgumentException("can only pow to a whole number");
		}
		var n_ = n;
		if(n.signum() == 0) return x.signum() != 0 ? x.one() : x.NaN();
		else if(n.signum() < 0) n = n.neg();
		N y = x.one();
		while(n.compareTo(n.one()) > 0) {
			var nhalf = n.divmod(n.two());
			n = nhalf[0];
			if(nhalf[1].signum() != 0) y = mul.apply(x,y);
			x = mul.apply(x,x);
		}
		x = mul.apply(x,y);
		return n_.signum() >= 0 ? x : x.one().div(x);
	}
	
	static <N extends num<N>> N binarySearch(N min, N max, N target, Comparator<N> cmp) {
		while(min.compareTo(max) <= 0) {
			var mid = min.add(max).div(min.two());
			int tmp = cmp.compare(mid,target);
			if(tmp < 0) min = mid.add(mid.ulp());
			else if(tmp > 0) max = mid.sub(mid.ulp());
			else return mid;
		}
		return null;
	}
}
