package ring.math.numbers;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import ring.formatting.FormatSpecifier;
import ring.math.numbers.IntBase.IBase;
import ring.math.numbers.IntBase.SBase;

interface FloatBase<N extends FloatBase<N>> extends IBase<N>, SBase<N> {
//	constants
	
	@Override default boolean isInt() { return false; }
	
//	basic arithmetic
	
	@Override default N mod(N n) { return NaN(); }
	
//	round
	
	@Override default N cut() { return signum() >= 0 ? floor() : ceil(); }
	
//	bit test
	
	@Override default int numberOfLeadingZeros() { throw new UnsupportedOperationException(); }
	@Override default int numberOfTrailingZeros() { throw new UnsupportedOperationException(); }
	@Override default int bitCount() { throw new UnsupportedOperationException(); }
	@Override default boolean bit(int i) { throw new UnsupportedOperationException(); }
	@Override default N setBit(int i, boolean b) { throw new UnsupportedOperationException(); }
	@Override default int indexOfBit(int nth) { throw new UnsupportedOperationException(); }
	
//	bitwise operations
	
	@Override default N and(N x) { throw new UnsupportedOperationException(); }
	@Override default N or(N x) { throw new UnsupportedOperationException(); }
	@Override default N xor(N x) { throw new UnsupportedOperationException(); }
	@Override default N not() { throw new UnsupportedOperationException(); }
	@Override default N notWithoutSign() { throw new UnsupportedOperationException(); }
	@Override default N shl(int amount) { throw new UnsupportedOperationException(); }
	@Override default N shr(int amount) { throw new UnsupportedOperationException(); }
	
//	Object overrides
	
	@Override default String toString(FormatSpecifier fmt) { return convertTo(bigfloat.class).toString(fmt); }
}
