package ring.math.numbers;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

interface Float4Base<N extends Float4Base<N>> extends FloatBase<N> {
	public static final int bitLength = 32;
	
	float rawValue();
	N createRaw(float value);
	
//	constants
	
	@Override default N zero() { return createRaw(0); }
	@Override default N one() { return createRaw(1); }
	@Override default N two() { return createRaw(2); }
	@Override default N minValue() { return createRaw(-Float.MAX_VALUE); }
	@Override default N maxValue() { return createRaw(Float.MAX_VALUE); }
	@Override default N negInfinity() { return createRaw(Float.NEGATIVE_INFINITY); }
	@Override default N posInfinity() { return createRaw(Float.POSITIVE_INFINITY); }
	@Override default N NaN() { return createRaw(Float.NaN); }
	@Override default int bitLength() { return bitLength; }
	@Override default N ulp() { return createRaw(Math.ulp(rawValue())); }
	
//	conversion
	
	@Override default boolean isNaN() { return Float.isNaN(rawValue()); }
	@Override default boolean isIrregular() { return !Float.isFinite(rawValue()); }
	
//	basic arithmetic
	
	@Override default N add(N n) { return createRaw(rawValue() + n.rawValue()); }
	@Override default N sub(N n) { return createRaw(rawValue() - n.rawValue()); }
	@Override default N mul(N n) { return createRaw(rawValue() * n.rawValue()); }
	@Override default N div(N n) { return createRaw(rawValue() / n.rawValue()); }
	
//	round
	
	@Override default N floor() { return createRaw((float)Math.floor(rawValue())); }
	@Override default N ceil() { return createRaw((float)Math.ceil(rawValue())); }
	@Override default N round() { return createRaw(Math.round(rawValue())); }
	
//	Object overrides
	
	default int hashCode0() { return Float.floatToRawIntBits(rawValue()); }
	@Override default int compareTo(N n) { return Float.compare(rawValue(),n.rawValue()); }
}
