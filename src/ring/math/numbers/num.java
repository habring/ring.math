package ring.math.numbers;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.lang.reflect.Array;
import java.util.Set;

import ring.formatting.Formattable;

public interface num<N extends num<N>> extends Comparable<N>, Formattable {
	public static final int maxRadix = ('z'-'a') + ('9'-'0') + 2;
	
//	constants
	
	N zero(); N one(); N two();
	N minValue(); N maxValue();
	N negInfinity(); N posInfinity();
	N NaN();
	/**-1 means infinite*/ 
	int bitLength();
	/**unit in the last place*/
	N ulp();
	/**True if there is no valid number between two whole numbers for this number type.*/
	boolean isInt();
	static int checkRadix(int radix) {
		if(1 < radix && radix <= maxRadix) return radix;
		throw new IllegalArgumentException("invalid radix "+radix);
	}
	@SuppressWarnings("unchecked")
	default N This() { return (N)this; }
	
//	conversion
	
	/**This method is needed because NaN != NaN by definition.*/
	boolean isNaN();
	boolean isIrregular();
	boolean isSigned();
	boolean isStrict();
	static num<?> wrap(Number n) {
		var ret = wrapOrNull(n);
		if(ret != null) return ret;
		throw new IllegalArgumentException("cannot convert type "+n.getClass());
	}
	static num<?> wrapOrNull(Number n) { return NumberConverters.wrap(n); }
	/**Converts to the nearest number.*/
	default <T extends num<T>> T convertTo(Class<T> type) { return NumberConverters.convert(This(),type); }
	default <T extends num<T>> T convertToLossless(Class<T> type) {
		var ret = convertTo(type); return isLosslessConverted(This(),ret) ? ret : null;
	}
	@SuppressWarnings("unchecked")
	default <A extends num<A>,B extends num<B>> boolean isLosslessConverted(A from, B to) {
		return from.equals(to.convertTo((Class<A>)from.getClass()));
	}
	/**Converts byte-by-byte (cutting off the most-significant bits if necessary).*/
	default <T extends num<T>> T convertToRaw(Class<T> type) { return NumberConverters.convertRaw(This(),type); }
	/**A constant set containing all classes where converting some value to this class is ALWAYS lossless.*/
	Set<Class<?>> containedClasses();
	default boolean containsType(Class<?> cls) { return containedClasses().contains(cls); }
	
//	basic arithmetic
	
	N add(N x); N sub(N x);
	N mul(N x); N div(N x); N mod(N x);
	@SuppressWarnings("unchecked")
	default N[] divmod(N x) {
		var ret = (N[])Array.newInstance(x.getClass(),2);
		ret[0] = div(x); ret[1] = mod(x); return ret;
	}
	default N neg() { return zero().sub(This()); }
	default N abs() { return signum() >= 0 ? This() : neg(); }
	default N min(N n) { return compareTo(n) <= 0 ? This() : n; }
	default N max(N n) { return compareTo(n) >= 0 ? This() : n; }
	default int signum() { return compareTo(zero()); }
	default N setSignum(int signum) { N tmp = abs(); return signum > 0 ? tmp : signum < 0 ? tmp.neg() : zero(); }
	
//	round
	
	N floor(); N ceil();
	N round(); N cut();
	default N roundUlp() { return bit(0) == (signum()>=0) ? shr(1).add(one().setSignum(signum())) : shr(1); }
	
//	bit test
	
	/**-1 means infinite (if and only if bitLength() == -1)*/
	int numberOfLeadingZeros();
	int numberOfTrailingZeros();
	/**The number of 1-bits in 2s complement binary form.*/
	int bitCount();
	default boolean isPowerOf2() { return bitCount() == 1 && signum() > 0; }
	boolean bit(int i);
	N setBit(int i, boolean b);
	/**Returns the index of the n-th set bit. -1 if not found.*/
	int indexOfBit(int nth);
	
//	bitwise operations
	
	N and(N x);
	N or(N x);
	N xor(N x);
	N not();
	/**Flips all bits except the sign-bit.*/
	N notWithoutSign();
	N shl(int amount);
	N shr(int amount);
	
//	complex operations
	
	default N pow(N pow) { return Helper.pow(This(),(x,y)->x.mul(y),pow); }
	default N log(N base) { return base.compareTo(one()) <= 0 ? NaN() : log2().div(base.log2()); }
	@SuppressWarnings("unchecked")
	default N log2() {
		var x2 = convertTo(bigfloat.class);
		x2.roundToF8();
		var p = zero().bitLength(); var f = convertTo(bigfloat.class);
		if(p >= i8.bitLength) f = f.setMaxPrecision(p);
		var f2 = f.log2();
		return f2.convertTo((Class<N>)getClass());
	}
	default N sqrt() { return nroot(two()); }
	default N nroot(N x) { return Helper.nroot(This(),x); }
	
//	Object overrides
	
	@Override int hashCode();
	@SuppressWarnings("unchecked")
	default boolean equals0(Object obj) { return obj != null && getClass() == obj.getClass() && compareTo((N)obj) == 0; }
	@Override int compareTo(N x);
	@Override String toString();
}
