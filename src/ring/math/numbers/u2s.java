package ring.math.numbers;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.Set;

import ring.formatting.FormatSpecifier;
import ring.math.numbers.Int2Base.S2Base;
import ring.math.numbers.Int2Base.U2Base;

public final class u2s implements U2Base<u2s>, S2Base<u2s> {
	private final short value;
	
	public u2s(short value) { this.value = value; }
	
	@Override public short rawValue() { return value; }
	
	@Override public u2s createRaw(short value) { return new u2s(value); }
	
//	conversion
	
	@Override public Set<Class<?>> containedClasses() { return ContainedClasses._u2s; }
	
//	basic arithmetic
	
	@Override
	public u2s div(u2s n) {
		int x = rawValue()&0xffff, y = n.rawValue()&0xffff;
		if(Integer.remainderUnsigned(x,y) == 0) return createRaw(Integer.divideUnsigned(x,y));
		throw new ArithmeticException(this+" % "+n+" != 0");
	}
	
//	Object overrides
	
	@Override public int hashCode() { return hashCode0(); }
	
	@Override public boolean equals(Object obj) { return equals0(obj); }
	
	@Override public String toString() { return toString(FormatSpecifier.string); }
}
