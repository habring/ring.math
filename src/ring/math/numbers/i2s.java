package ring.math.numbers;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.Set;

import ring.formatting.FormatSpecifier;
import ring.math.numbers.Int2Base.I2Base;
import ring.math.numbers.Int2Base.S2Base;

public final class i2s implements I2Base<i2s>, S2Base<i2s> {
	private final short value;
	
	public i2s(short value) { this.value = value; }
	
	@Override public short rawValue() { return value; }
	
	@Override public i2s createRaw(short value) { return new i2s(value); }
	
//	conversion
	
	@Override public Set<Class<?>> containedClasses() { return ContainedClasses._i2s; }
	
//	basic arithmetic
	
	@Override
	public i2s div(i2s n) {
		int x = rawValue(), y = n.rawValue();
		if(x % y == 0) return createRaw(x / y);
		throw new ArithmeticException(this+" % "+n+" != 0");
	}
	
//	Object overrides
	
	@Override public int hashCode() { return hashCode0(); }
	
	@Override public boolean equals(Object obj) { return equals0(obj); }
	
	@Override public String toString() { return toString(FormatSpecifier.string); }
}
