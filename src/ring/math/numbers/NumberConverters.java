package ring.math.numbers;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.*;
import java.util.function.Function;

import ring.math.numbers.Int1Base.S1Base;
import ring.math.numbers.Int1Base.W1Base;
import ring.math.numbers.Int2Base.S2Base;
import ring.math.numbers.Int2Base.W2Base;
import ring.math.numbers.Int4Base.S4Base;
import ring.math.numbers.Int4Base.W4Base;
import ring.math.numbers.Int8Base.S8Base;
import ring.math.numbers.Int8Base.W8Base;
import ring.util.Converter;
import ring.util.Converters;

class NumberConverters {
	static final ErrorMessages errors = new ErrorMessages();
	private static final Converters convert, convertRaw;
	private static final Map<Class<?>,Class<?>> jMap;
	
	static {
		var b = new Builder(); b.build(); convert = new Converters(b.convList); convertRaw = new Converters(b.rawConvList);
		var tmp = new HashMap<Class<?>,Class<?>>();
		tmp.put(byte.class,i1.class);	tmp.put(Byte.class,i1.class);
		tmp.put(short.class,i2.class);	tmp.put(Short.class,i2.class);
		tmp.put(int.class,i4.class);	tmp.put(Integer.class,i4.class);
		tmp.put(long.class,i8.class);	tmp.put(Long.class,i8.class);
		tmp.put(float.class,f4.class);	tmp.put(Float.class,f4.class);
		tmp.put(double.class,f8.class);	tmp.put(Double.class,f8.class);
		jMap = Collections.unmodifiableMap(tmp);
	}
	
	static <A,B> B convert(A a, Class<B> b) { return convert(convert,a,b); }
	
	static <A,B> B convertRaw(A a, Class<B> b) { return convert(convertRaw,a,b); }
	
	@SuppressWarnings("unchecked")
	private static <A,B> B convert(Converters conv, A a, Class<B> b) {
		var tmp = conv.getOrNull((Class<A>)a.getClass(),b); return tmp == null ? null : tmp.convert(a);
	}
	
	static num<?> wrap(Number n) {
		var tmp = jMap.get(n.getClass());
		return tmp == null ? null : (num<?>)convert(n,tmp);
	}
	
	private static class Builder {
		final List<Converter<?,?>> convList = new ArrayList<>(), rawConvList = new ArrayList<>();
		
		void build() {
			var i1 = new i1((byte)0);		var i2 = new i2((short)0);		var i4 = new i4(0);		var i8 = new i8(0);
			var i1s = new i1s((byte)0);		var i2s = new i2s((short)0);	var i4s = new i4s(0);	var i8s = new i8s(0);
			var u1 = new u1((byte)0);		var u2 = new u2((short)0);		var u4 = new u4(0);		var u8 = new u8(0);
			var u1s = new u1s((byte)0);		var u2s = new u2s((short)0);	var u4s = new u4s(0);	var u8s = new u8s(0);
			var f4 = new f4(0);				var f8 = new f8(0);				var bi = new bigint(0);	var bf = new bigfloat(0);
//			26^2 = 676 combinations to convert, 38 calls in here, 35 implementations, max 7 hops
			l(i1,i2); l(u1,u2);		l(i2,i4); l(u2,u4);		l(i4,i8); l(u4,u8);		l(f4,f8);
			u(i1,u1); u(i2,u2); u(i4,u4); u(i8,u8);			ul(u1,i2); ul(u2,i4); ul(u4,i8);
			s(i1,i1s); s(u1,u1s);	s(i2,i2s); s(u2,u2s);	s(i4,i4s); s(u4,u4s);	s(i8,i8s); s(u8,u8s);
			f(i4,f4); f(i8,f8); f(u8,f8);		j(i1); j(i2); j(i4); j(i8); j(f4); j(f8);
			b(i8,bi); b(u8,bi); b(f8,bi);		b(i8,bf); b(u8,bf); b(f8,bf);		b(bi,bf);
		}
		
		void l(i1 a, i2 b) {
			add(	c(a,b,x->b.createRaw(x.rawValue()),x->{ var y = x.rawValue(); var z = (byte)y; return y==z?a.createRaw(z):y<0?a.minValue():a.maxValue(); }),
					c(a,b,x->b.createRaw(x.rawValue()),x->a.createRaw(x.rawValue()))	);
		}
		void l(i2 a, i4 b) {
			add(	c(a,b,x->b.createRaw(x.rawValue()),x->{ var y = x.rawValue(); var z = (short)y; return y==z?a.createRaw(z):y<0?a.minValue():a.maxValue(); }),
					c(a,b,x->b.createRaw(x.rawValue()),x->a.createRaw(x.rawValue()))	);
		}
		void l(i4 a, i8 b) {
			add(	c(a,b,x->b.createRaw(x.rawValue()),x->{ var y = x.rawValue(); var z = (int)y; return y==z?a.createRaw(z):y<0?a.minValue():a.maxValue(); }),
					c(a,b,x->b.createRaw(x.rawValue()),x->a.createRaw((int)(x.rawValue()&0xffffffffL)))		);
		}
		void l(u1 a, u2 b) {
			add(	c(a,b,x->b.createRaw(x.rawValue()&0xff),x->{ var y = x.rawValue(); var z = (byte)(y&0xff); return y==z?a.createRaw(z):a.maxValue(); }),
					c(a,b,x->b.createRaw(x.rawValue()&0xff),x->a.createRaw(x.rawValue()))	);
		}
		void l(u2 a, u4 b) {
			add(	c(a,b,x->b.createRaw(x.rawValue()&0xffff),x->{ var y = x.rawValue(); var z = (short)(y&0xffff); return y==z?a.createRaw(z):a.maxValue(); }),
					c(a,b,x->b.createRaw(x.rawValue()&0xffff),x->a.createRaw(x.rawValue()))	);
		}
		void l(u4 a, u8 b) {
			add(	c(a,b,x->b.createRaw(x.rawValue()&0xffffffffL),x->{ var y = x.rawValue(); var z = (int)(y&0xffffffffL); return y==z?a.createRaw(z):a.maxValue(); }),
					c(a,b,x->b.createRaw(x.rawValue()&0xffffffffL),x->a.createRaw((int)(x.rawValue()&0xffffffffL)))		);
		}
		void l(f4 a, f8 b) { add(c(a,b,x->b.createRaw(x.rawValue()),x->a.createRaw((float)x.rawValue())), null); }
		
		void u(i1 i, u1 u) {
			add(	c(i,u,x->x.signum()>=0?u.createRaw(x.rawValue()):u.zero(),x->x.rawValue()>=0?i.createRaw(x.rawValue()):i.maxValue()),
					c(i,u,x->u.createRaw(x.rawValue()),x->i.createRaw(x.rawValue()))	);
		}
		void u(i2 i, u2 u) {
			add(	c(i,u,x->x.signum()>=0?u.createRaw(x.rawValue()):u.zero(),x->x.rawValue()>=0?i.createRaw(x.rawValue()):i.maxValue()),
					c(i,u,x->u.createRaw(x.rawValue()),x->i.createRaw(x.rawValue()))	);
		}
		void u(i4 i, u4 u) {
			add(	c(i,u,x->x.signum()>=0?u.createRaw(x.rawValue()):u.zero(),x->x.rawValue()>=0?i.createRaw(x.rawValue()):i.maxValue()),
					c(i,u,x->u.createRaw(x.rawValue()),x->i.createRaw(x.rawValue()))	);
		}
		void u(i8 i, u8 u) {
			add(	c(i,u,x->x.signum()>=0?u.createRaw(x.rawValue()):u.zero(),x->x.rawValue()>=0?i.createRaw(x.rawValue()):i.maxValue()),
					c(i,u,x->u.createRaw(x.rawValue()),x->i.createRaw(x.rawValue()))	);
		}
		
		void ul(u1 u, i2 i) {
			add(	c(u,i,x->i.createRaw(x.rawValue()&u.maxValue().rawValue()),x->{ var y = x.rawValue(); return y < 0 ? u.zero() : y <= u.maxValue().rawValue() ? u.createRaw(y) : u.maxValue(); }),
					c(u,i,x->i.createRaw(x.rawValue()),x->u.createRaw(x.rawValue()))	);
		}
		void ul(u2 u, i4 i) {
			add(	c(u,i,x->i.createRaw(x.rawValue()&u.maxValue().rawValue()),x->{ var y = x.rawValue(); return y < 0 ? u.zero() : y <= u.maxValue().rawValue() ? u.createRaw(y) : u.maxValue(); }),
					c(u,i,x->i.createRaw(x.rawValue()),x->u.createRaw(x.rawValue()))	);
		}
		void ul(u4 u, i8 i) {
			add(	c(u,i,x->i.createRaw(x.rawValue()&u.maxValue().rawValue()),x->{ var y = x.rawValue(); return y < 0 ? u.zero() : y <= u.maxValue().rawValue() ? u.createRaw((int)(y&0xffffffffL)) : u.maxValue(); }),
					c(u,i,x->i.createRaw(x.rawValue()),x->u.createRaw((int)(x.rawValue()&0xffffffffL)))	);
		}
		
		<A extends W1Base<A>,B extends S1Base<B>> void s(A w, B s) { add(c(w,s,x->s.createRaw(x.rawValue()),x->w.createRaw(x.rawValue()))); }
		<A extends W2Base<A>,B extends S2Base<B>> void s(A w, B s) { add(c(w,s,x->s.createRaw(x.rawValue()),x->w.createRaw(x.rawValue()))); }
		<A extends W4Base<A>,B extends S4Base<B>> void s(A w, B s) { add(c(w,s,x->s.createRaw(x.rawValue()),x->w.createRaw(x.rawValue()))); }
		<A extends W8Base<A>,B extends S8Base<B>> void s(A w, B s) { add(c(w,s,x->s.createRaw(x.rawValue()),x->w.createRaw(x.rawValue()))); }
		
		void f(i4 i, f4 f) { add(null, c(i,f,x->f.createRaw(Float.intBitsToFloat(x.rawValue())),x->i.createRaw(Float.floatToRawIntBits(x.rawValue())))); }
		void f(i8 i, f8 f) { add(c(i,f,x->f.createRaw(x.rawValue()),x->x.isNaN()?null:i.createRaw(Math.round(x.rawValue()))), cr(i,f)); }
		void f(u8 u, f8 f) { add(c(u,f,x->new bigfloat(new bigint(x)).roundToF8(),x->new bigfloat(x.rawValue()).roundToBigInt().roundToU8()), cr(u,f)); }
		
		void b(i8 i, bigint bi) { add(cb(i,bi)); }
		void b(u8 u, bigint bi) { add(cb(u,bi)); }
		void b(f8 f, bigint bi) { var i = new i8(0); var bf = new bigfloat(0); add(cb(f,bf).andThen(cb(bi,bf).inverse()), cr(i,f).inverse().andThen(cb(i,bi))); }
		void b(i8 i, bigfloat bf) { var bi = new bigint(0); add(cb(i,bi).andThen(cb(bi,bf)), null); }
		void b(u8 u, bigfloat bf) { var bi = new bigint(0); add(cb(u,bi).andThen(cb(bi,bf)), null); }
		void b(f8 f, bigfloat bf) { add(cb(f,bf), null); }
		void b(bigint bi, bigfloat bf) { add(cb(bi,bf), null); }
		
		void j(i1 i) { add(c(Byte.class,i,x->i.createRaw(x),x->x.rawValue())); }
		void j(i2 i) { add(c(Short.class,i,x->i.createRaw(x),x->x.rawValue())); }
		void j(i4 i) { add(c(Integer.class,i,x->i.createRaw(x),x->x.rawValue())); }
		void j(i8 i) { add(c(Long.class,i,x->i.createRaw(x),x->x.rawValue())); }
		void j(f4 f) { add(c(Float.class,f,x->f.createRaw(x),x->x.rawValue())); }
		void j(f8 f) { add(c(Double.class,f,x->f.createRaw(x),x->x.rawValue())); }
		
		static <A extends Int8Base<A>> Converter<A,f8> cr(A i, f8 f) {
			return c(i,f,x->f.createRaw(Double.longBitsToDouble(x.rawValue())),x->i.createRaw(Double.doubleToRawLongBits(x.rawValue())));
		}
		static Converter<i8,bigint> cb(i8 i, bigint bi) { return c(i,bi,x->new bigint(x.rawValue()),x->x.roundToI8()); }
		static Converter<u8,bigint> cb(u8 u, bigint bi) { return c(u,bi,x->new bigint(x),x->x.roundToU8()); }
		static Converter<f8,bigfloat> cb(f8 f, bigfloat bf) { return c(f,bf,x->new bigfloat(x.rawValue()),x->x.roundToF8()); }
		static Converter<bigint,bigfloat> cb(bigint bi, bigfloat bf) { return c(bi,bf,x->new bigfloat(x),x->x.roundToBigInt()); }
		@SuppressWarnings("unchecked")
		static <A extends num<A>,B extends num<B>> Converter<A,B> c(A from, B to, Function<A,B> conv, Function<B,A> rev) {
			return new Converter<>((Class<A>)from.getClass(),(Class<B>)to.getClass()) {
				@Override public B convert(A obj) { return conv.apply(obj); }
				@Override public A revert(B obj) { return rev.apply(obj); }
			};
		}
		@SuppressWarnings("unchecked")
		static <A extends Number, B extends num<B>> Converter<A,B> c(Class<A> from, B to, Function<A,B> conv, Function<B,A> rev) {
			return new Converter<>(from,(Class<B>)to.getClass()) {
				@Override public B convert(A obj) { return conv.apply(obj); }
				@Override public A revert(B obj) { return rev.apply(obj); }
			};
		}
		
		void add(Converter<?,?> conv) { add(conv,conv); }
		void add(Converter<?,?> c, Converter<?,?> r) {
			if(c != null && r != null && (c.fromType != r.fromType || c.toType != r.toType)) throw new Error(errors.unreachable);
			if(c != null) this.convList.add(c);
			if(r != null) this.rawConvList.add(r);
		}
	}
}
