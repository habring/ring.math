package ring.math.numbers;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

interface Int2Base<N extends Int2Base<N>> extends IntBase<N> {
	public static final int bitLength = 16;
	
	short rawValue();
	N createRaw(short value);
	default N createRaw(int value) { return createRaw((short)(value&0xffff)); }
	
//	constants
	
	@Override default N zero() { return createRaw(0); }
	@Override default N one() { return createRaw(1); }
	@Override default N two() { return createRaw(2); }
	@Override default int bitLength() { return bitLength; }
	
//	bit test
	
	@Override
	default int numberOfLeadingZeros() {
		short x = rawValue();
		if(x == 0) return bitLength;
		int n = 0;
		if((x&0xff00) == 0) { n+=8; x<<=8; }
		if((x&0xf000) == 0) { n+=4; x<<=4; }
		if((x&0xc000) == 0) { n+=2; x<<=2; }
		if((x&0x8000) == 0) n++;
		return n;
	}
	@Override
	default int numberOfTrailingZeros() {
		short x = rawValue();
		if(x == 0) return bitLength;
		int n = 0;
		if((x&0x00ff) == 0) { n+=8; x>>=8; }
		if((x&0x000f) == 0) { n+=4; x>>=4; }
		if((x&0x0003) == 0) { n+=2; x>>=2; }
		if((x&0x0001) == 0) n++;
		return n;
	}
	@Override
	default int bitCount() {
		short x = rawValue(); x -= (x>>>1) & 0x5555;
		x = (short)((x&0x3333) + ((x>>>2) & 0x3333));
		x = (short)((x + (x>>>4)) & 0x0f0f);
		x+=x>>>8; return x&0x1f;
	}
	@Override default boolean bit(int i) { return (rawValue()&(1<<i)) != 0; }
	@Override default N setBit(int i, boolean b) { return createRaw(b ? rawValue()|(1<<i) : rawValue()&(~(1<<i))); }
	@Override
	default int indexOfBit(int n) {
		if(n < 0) throw new IllegalArgumentException();
		short v = rawValue();
		i1 h = new i1((byte)(v>>>i1.bitLength)), l = new i1((byte)(v&0xff));
		var x = l.bitCount();
		if(n < x) return l.indexOfBit(n);
		n-=x; return i1.bitLength + h.indexOfBit(n);
	}
	
//	bitwise operations
	
	@Override default N and(N x) { return createRaw(rawValue() & x.rawValue()); }
	@Override default N or(N x) { return createRaw(rawValue() | x.rawValue()); }
	@Override default N xor(N x) { return createRaw(rawValue() ^ x.rawValue()); }
	@Override default N not() { return createRaw(~rawValue()); }
	@Override default N notWithoutSign() { return createRaw(((~rawValue())&maxValue().rawValue()) | (rawValue()&minValue().rawValue())); }
	@Override default N shl(int amount) { return createRaw(rawValue() << amount); }
	
//	Object overrides
	
	default int hashCode0() { return rawValue(); }
	
	
	
	static interface I2Base<N extends I2Base<N>> extends Int2Base<N>, IBase<N> {
//		constants
		
		@Override default N minValue() { return createRaw(0x8000); }
		@Override default N maxValue() { return createRaw(0x7fff); }
		
//		basic arithmetic
		
		@Override default N mod(N n) { return createRaw(rawValue() % n.rawValue()); }
		
	//	bitwise operations
		
		@Override default N shr(int amount) { return createRaw(rawValue() >> amount); }
		
	//	Object overrides
		
		@Override default int compareTo(N n) { return rawValue() < n.rawValue() ? -1 : rawValue() > n.rawValue() ? 1 : 0; }
	}
	
	static interface U2Base<N extends U2Base<N>> extends Int2Base<N>, UBase<N> {
//		constants
		
		@Override default N maxValue() { return createRaw(0xffff); }
		
//		basic arithmetic
		
		@Override default N mod(N n) { return createRaw(Integer.remainderUnsigned(rawValue()&0xffff,n.rawValue()&0xffff)); }
		
	//	bitwise operations
		
		@Override default N shr(int amount) { return createRaw(rawValue() >>> amount); }
		
	//	Object overrides
		
		@Override default int compareTo(N n) { return Short.compareUnsigned(rawValue(),n.rawValue()); }
	}
	
	static interface S2Base<N extends S2Base<N>> extends Int2Base<N>, SBase<N> {
//		basic arithmetic
		
		@Override default N add(N n) { return createRaw(Math.addExact(rawValue(),n.rawValue())); }
		@Override default N sub(N n) { return createRaw(Math.subtractExact(rawValue(),n.rawValue())); }
		@Override default N mul(N n) { return createRaw(Math.multiplyExact(rawValue(),n.rawValue())); }
	}
	
	static interface W2Base<N extends W2Base<N>> extends Int2Base<N>, WBase<N> {
//		basic arithmetic
		
		@Override default N add(N n) { return createRaw(rawValue() + n.rawValue()); }
		@Override default N sub(N n) { return createRaw(rawValue() - n.rawValue()); }
		@Override default N mul(N n) { return createRaw(rawValue() * n.rawValue()); }
	}
}
