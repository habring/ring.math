package ring.math.numbers;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import ring.formatting.FormatSpecifier;

interface IntBase<N extends IntBase<N>> extends num<N> {
//	constants
	
	@Override default N negInfinity() { return null; }
	@Override default N posInfinity() { return null; }
	@Override default N NaN() { return null; }
	@Override default N ulp() { return one(); }
	@Override default boolean isInt() { return true; }
	
//	conversion
	
	@Override default boolean isNaN() { return false; }
	@Override default boolean isIrregular() { return false; }
	
//	round
	
	@Override default N floor() { return This(); }
	@Override default N ceil() { return This(); }
	@Override default N round() { return This(); }
	@Override default N cut() { return This(); }
	
//	Object overrides
	
	@Override default String toString(FormatSpecifier fmt) { return convertTo(bigfloat.class).toString(fmt); }
	
	
	
	static interface IBase<N extends IBase<N>> extends num<N> {
//		conversion
		
		@Override default boolean isSigned() { return true; }
	}
	
	static interface UBase<N extends UBase<N>> extends num<N> {
//		constants
		
		@Override default N minValue() { return zero(); }
		
//		conversion
		
		@Override default boolean isSigned() { return false; }
	}
	
	static interface SBase<N extends SBase<N>> extends num<N> {
//		conversion
		
		@Override default boolean isStrict() { return true; }
	}
	
	static interface WBase<N extends WBase<N>> extends num<N> {
//		conversion
		
		@Override default boolean isStrict() { return false; }
	}
}
