package ring.math.numbers;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

interface Int4Base<N extends Int4Base<N>> extends IntBase<N> {
	public static final int bitLength = 32;
	
	int rawValue();
	N createRaw(int value);
	
//	constants
	
	@Override default N zero() { return createRaw(0); }
	@Override default N one() { return createRaw(1); }
	@Override default N two() { return createRaw(2); }
	@Override default int bitLength() { return bitLength; }
	
//	bit test
	
	@Override
	default int numberOfLeadingZeros() {
		int x = rawValue();
		if(x == 0) return bitLength;
		int n = 0;
		if((x&0xffff0000) == 0) { n+=16; x<<=16; }
		if((x&0xff000000) == 0) { n+=8; x<<=8; }
		if((x&0xf0000000) == 0) { n+=4; x<<=4; }
		if((x&0xc0000000) == 0) { n+=2; x<<=2; }
		if((x&0x80000000) == 0) n++;
		return n;
	}
	@Override
	default int numberOfTrailingZeros() {
		int x = rawValue();
		if(x == 0) return bitLength;
		int n = 0;
		if((x&0x0000ffff) == 0) { n+=16; x>>=16; }
		if((x&0x000000ff) == 0) { n+=8; x>>=8; }
		if((x&0x0000000f) == 0) { n+=4; x>>=4; }
		if((x&0x00000003) == 0) { n+=2; x>>=2; }
		if((x&0x00000001) == 0) n++;
		return n;
	}
	@Override
	default int bitCount() {
		int r = rawValue(); r -= (r>>>1) & 0x55555555;
		r = (r&0x33333333) + ((r>>>2) & 0x33333333);
		r = (r + (r>>>4)) & 0x0f0f0f0f;
		r+=r>>>8; r+=r>>>16; return r&0x3f;
	}
	@Override default boolean bit(int i) { return (rawValue()&(1<<i)) != 0; }
	@Override default N setBit(int i, boolean b) { return createRaw(b ? rawValue()|(1<<i) : rawValue()&(~(1<<i))); }
	@Override
	default int indexOfBit(int n) {
		if(n < 0) throw new IllegalArgumentException();
		int v = rawValue();
		i2 h = new i2((short)(v>>>i2.bitLength)), l = new i2((short)(v&0xffff));
		var x = l.bitCount();
		if(n < x) return l.indexOfBit(n);
		n-=x; return i2.bitLength + h.indexOfBit(n);
	}
	
//	bitwise operations
	
	@Override default N and(N x) { return createRaw(rawValue() & x.rawValue()); }
	@Override default N or(N x) { return createRaw(rawValue() | x.rawValue()); }
	@Override default N xor(N x) { return createRaw(rawValue() ^ x.rawValue()); }
	@Override default N not() { return createRaw(~rawValue()); }
	@Override default N notWithoutSign() { return createRaw(((~rawValue())&maxValue().rawValue()) | (rawValue()&minValue().rawValue())); }
	@Override default N shl(int amount) { return createRaw(rawValue() << amount); }
	
//	Object overrides
	
	default int hashCode0() { return rawValue(); }
	
	
	
	static interface I4Base<N extends I4Base<N>> extends Int4Base<N>, IBase<N> {
//		constants
		
		@Override default N minValue() { return createRaw(0x80000000); }
		@Override default N maxValue() { return createRaw(0x7fffffff); }
		
//		basic arithmetic
		
		@Override default N mod(N n) { return createRaw(rawValue() % n.rawValue()); }
		
	//	bitwise operations
		
		@Override default N shr(int amount) { return createRaw(rawValue() >> amount); }
		
	//	Object overrides
		
		@Override default int compareTo(N n) { return rawValue() < n.rawValue() ? -1 : rawValue() > n.rawValue() ? 1 : 0; }
	}
	
	static interface U4Base<N extends U4Base<N>> extends Int4Base<N>, UBase<N> {
//		constants
		
		@Override default N maxValue() { return createRaw(0xffffffff); }
		
//		basic arithmetic
		
		@Override default N mod(N n) { return createRaw(Integer.remainderUnsigned(rawValue(),n.rawValue())); }
		
	//	bitwise operations
		
		@Override default N shr(int amount) { return createRaw(rawValue() >>> amount); }
		
	//	Object overrides
		
		@Override default int compareTo(N n) { return Integer.compareUnsigned(rawValue(),n.rawValue()); }
	}
	
	static interface S4Base<N extends S4Base<N>> extends Int4Base<N>, SBase<N> {
//		basic arithmetic
		
		@Override default N add(N n) { return createRaw(Math.addExact(rawValue(),n.rawValue())); }
		@Override default N sub(N n) { return createRaw(Math.subtractExact(rawValue(),n.rawValue())); }
		@Override default N mul(N n) { return createRaw(Math.multiplyExact(rawValue(),n.rawValue())); }
	}
	
	static interface W4Base<N extends W4Base<N>> extends Int4Base<N>, WBase<N> {
//		basic arithmetic
		
		@Override default N add(N n) { return createRaw(rawValue() + n.rawValue()); }
		@Override default N sub(N n) { return createRaw(rawValue() - n.rawValue()); }
		@Override default N mul(N n) { return createRaw(rawValue() * n.rawValue()); }
	}
}
