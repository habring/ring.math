package ring.math.numbers;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.Arrays;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;

import ring.formatting.FormatSpecifier;
import ring.math.numbers.IntBase.IBase;
import ring.math.numbers.IntBase.SBase;

public final class bigint implements IntBase<bigint>, IBase<bigint>, SBase<bigint> {
	static final ErrorMessages errors = new ErrorMessages();
	private static final bigint zero = new bigint(0), one = new bigint(1), two = new bigint(2);
	/**signum: -1, 0, 1*/ private final int sig;
	/**magnitude (unsigned)*/ private final int[] mag;
	
	private bigint(long sig, int[] mag) {
		this.mag = removeLeadingZeros(mag);
		this.sig = this.mag.length == 0 ? 0 : sig >= 0 ? 1 : -1;
	}
	public bigint(long value) { this(value,ui64ToMag(Math.abs(value))); }
	bigint(u8 value) { this(1,ui64ToMag(value.rawValue())); }
	/**creates a bigint from 2s complement.*/
	private bigint(int[] x) { this(x[0],absOf2sComplementUnsafe(x)); }
	
	private static int[] absOf2sComplementUnsafe(int[] x) {
		if(x.length == 0 || x[0] >= 0) return x;
		for(int i = 0; i < x.length; i++) x[i] = ~x[i];
		return addUnsafe(x,new int[]{1});
	}
	private int[] to2sComplementUnsafe() {
		if(sig >= 0) return mag.length == 0 || mag[0] < 0 ? grow(mag,mag.length+1) : mag;
		else {
			int[] ret = Arrays.copyOf(mag,mag.length);
			for(int i = 0; i < ret.length; i++) ret[i] = ~ret[i];
			return addUnsafe(ret,new int[]{1});
		}
	}
	private static int[] ui64ToMag(long x) {
		if((int)x == x && x >= 0) return new int[] { (int)x };
		return new int[]{(int)(x>>>32),(int)(x&0xffffffff)};
	}
	long magToUi64() { return magToUi64(mag); }
	private static long magToUi64(int[] mag) {
		switch(mag.length) {
		case 0: return 0;
		case 1: return mag[0]&0xffffffffL;
		case 2: return ((long)mag[0])<<32 | (mag[1]&0xffffffffL);
		default: throw new Error(errors.unreachable);
		}
	}
	private static int[] grow(int[] a, int length) {
		if(a.length >= length) return a;
		var ret = new int[length];
		System.arraycopy(a,0,ret,length-a.length,a.length);
		return ret;
	}
	private static int[] removeLeadingZeros(int[] a) {
		int i;
		for(i = 0; i < a.length && a[i] == 0; i++) {}
		if(i == 0) return a;
		int[] b = new int[a.length-i];
		System.arraycopy(a,i,b,0,b.length);
		return b;
	}
	
	
	
//	constants
	
	@Override public bigint zero() { return zero; }
	@Override public bigint one() { return one; }
	@Override public bigint two() { return two; }
	@Override public bigint minValue() { return null; }
	@Override public bigint maxValue() { return null; }
	@Override
	public int bitLength() {
		int ret = 1;
		if(sig != 0) {
			ret = mag.length*32 - new i4(mag[0]).numberOfLeadingZeros();
			if(sig > 0 || !isPowerOf2()) ret++;
		}
		return ret;
	}
	
	
	
//	conversion
	
	@Override public Set<Class<?>> containedClasses() { return ContainedClasses._bigint; }
	
	int[] toRadix(int radix) {
		if(sig < 0) throw new Error(errors.unreachable);
		num.checkRadix(radix);
		var radix2 = new bigint(radix);
		int[] ret = new int[new f8(mag.length * (double)i4.bitLength / BigFloatSerializer.radixLog2[radix]).convertTo(i4.class).rawValue()+1];
		var v = this;
		for(int i = ret.length-1; v.sig > 0 && i >= 0; i--) {
			var tmp = v.divmod(radix2);
			ret[i] = tmp[1].convertTo(i4.class).rawValue();
			v = tmp[0];
		}
		return removeLeadingZeros(ret);
	}
	
	
	
//	basic arithmetic
	
	@Override
	public bigint add(bigint n) {
		if(sig == 0) return n;
		else if(n.sig < 0) return sub(n.neg());
		else if(sig < 0) return n.sub(neg());
		var ret = addUnsafe(mag,n.mag);
		return new bigint(ret.length>0?1:0,ret);
	}
	@Override
	public bigint sub(bigint n) {
		if(sig == 0) return n.neg();
		else if(n.sig < 0) return add(n.neg());
		else if(sig < 0) return neg().add(n).neg();
		int cmp = this.compareTo(n);
		if(cmp < 0) return n.sub(this).neg();
		else if(cmp == 0) return zero();
		return new bigint(1,subUnsafe(mag,n.mag));
	}
	private static int[] addUnsafe(int[] a, int[] b) {
		if(a.length < b.length) return addUnsafe(b,a);
		b = grow(b,a.length);
		var ret = new int[a.length];
		long m = 0xffffffffL; long c = 0; int i;
		for(i = a.length-1; i >= 0; i--) {
			long x = (a[i]&m) + (b[i]&m) + c;
			ret[i] = (int)x; c = x>>>32;
		}
		if(c != 0) {
			var ret2 = new int[a.length+1];
			System.arraycopy(ret,0,ret2,1,ret.length);
			ret2[0] = (int)c; return ret2;
		}
		return ret;
	}
	/**expects a > b*/
	private static int[] subUnsafe(int[] a, int[] b) {
		b = grow(b,a.length);
		var ret = new int[a.length];
		long m = 0xffffffffL; long diff = 0;
		for(int i = a.length-1; i >= 0; i--) {
			diff = (a[i]&m) - (b[i]&m) + (diff>>32);
			ret[i] = (int)diff;
		}
		if((diff>>32) != 0) throw new Error(errors.unreachable);
		return ret;
	}
	@Override public bigint neg() { return new bigint(-sig,mag); }
	@Override public int signum() { return sig; }
	@Override public bigint setSignum(int signum) { return signum == 0 ? zero() : new bigint(signum,mag); }
	
	@Override public bigint mul(bigint n) { return mulKaratsuba(this,n); }
	/**http://en.wikipedia.org/wiki/Karatsuba_algorithm*/
	private static bigint mulKaratsuba(bigint a, bigint b) {
		bigint ret;
		if(a.sig == 0 || b.sig == 0) ret = new bigint(0);
		else if(a.mag.length == 1 && b.mag.length == 1) {
			long m = 0xffffffffL;
			ret = new bigint(1,ui64ToMag((a.mag[0]&m)*(b.mag[0]&m)));
		}
		else if(a.mag.length == 1) ret = b.mulSingleUnsignedUnsafe(a);
		else if(b.mag.length == 1) ret = a.mulSingleUnsignedUnsafe(b);
		else {
			var tmp = Math.max(a.bitLength(),b.bitLength()) - 1;
			int mid = Math.toIntExact(tmp/2);
			bigint[] x = a.abs().split(mid), y = b.abs().split(mid);
			var t = x[0].mul(y[0]);
			var u = x[1].mul(y[1]);
			var v = x[0].add(x[1]).mul(y[0].add(y[1])).sub(t).sub(u);
			ret = t.shl(mid).add(v).shl(mid).add(u);
		}
		return a.sig != b.sig ? ret.neg() : ret;
	}
	private bigint[] split(int bitIdx) {
		var mask = new bigint(0).setBit(bitIdx,true).notWithoutSign();
		return new bigint[]{shr(bitIdx),and(mask)};
	}
	private bigint mulSingleUnsignedUnsafe(bigint x) {
		if(x.mag.length != 1) throw new Error(errors.unreachable);
		int carry = 0;
		long m = 0xffffffffL, y = x.mag[0]&m;
		int[] r = new int[mag.length+1];
		for(int i = r.length-1; i > 0; i--) {
			long tmp = (mag[i-1]&m) * y + carry;
			r[i] = (int)tmp;
			carry = (int)(tmp>>>32);
		}
		r[0] = carry;
		return new bigint(1,r);
	}
	
	@Override public bigint div(bigint n) { return divmod(n)[0]; }
	@Override public bigint mod(bigint n) { return divmod(n)[1]; }
	@Override
	public bigint[] divmod(bigint n) {
		var ret = divmod(this,n,(q,r)->false);
		if(ret.length == 2) return ret;
		throw new Error(errors.unreachable);
	}
	static bigint[] divmod(bigint a, bigint b, BiPredicate<bigint,bigint> borrowBit) {
		if(b.sig == 0) throw new ArithmeticException("division by zero");
		if(a.sig == 0) return new bigint[]{a,a};
		int qsig = a.sig*b.sig, rsig = a.sig;
		var qbits = Math.max(a.bitLength()-b.bitLength(),0);
		a = a.abs(); var b_ = b.abs();
		b = b_.shl(qbits+1);
		if(a.bitLength() > b.bitLength()) throw new Error(errors.unreachable);
		var q = new bigint(0); int i = qbits;
		while(true) {
			if(i >= 0) {
				b = b.shr(1);
				if(i == 0 && cmp(b.mag,b_.mag) != 0) throw new Error(errors.unreachable);
			}
			else if(borrowBit.test(q,a)) a = a.shl(1);
			else break;
			q = q.shl(1);
			var bit = cmp(a.mag,b.mag) >= 0;
			if(bit) {
				q = q.or(new bigint(1));
				a = a.sub(b);
				if(a.sig == 0) {
					if(i > 0) q = q.shl(i);
					i = -1; break;
				}
			}
			i--;
		}
		if(q.sig < 0 || a.sig < 0 || i >= 0) throw new Error(errors.unreachable);
		q = new bigint(qsig,q.mag); a = new bigint(rsig,a.mag);
		return i == -1 ? new bigint[]{q,a} : new bigint[]{q,a,new bigint(-i-1)};
	}
	
	
	
//	round
	
	i8 roundToI8() {
		if(mag.length <= 2) {
			long tmp = magToUi64(mag);
			if(tmp >= 0) return new i8(tmp).setSignum(sig);
		}
		return sig >= 0 ? new i8(0).maxValue() : new i8(0).minValue();
	}
	u8 roundToU8() { return sig < 0 ? new u8(0) : mag.length > 2 ? new u8(0).maxValue() : new u8(magToUi64(mag)); }
	
	
	
//	bit test
	
	@Override public int numberOfLeadingZeros() { return 0; }
	@Override
	public int numberOfTrailingZeros() {
		return numberOfTrailingZerosUnsafe(to2sComplementUnsafe());
	}
	private static int numberOfTrailingZerosUnsafe(int[] v) {
		if(v.length == 1 && v[0] == 0) return 0;
		int ret = 0;
		for(int i = v.length-1; i >= 0; i--) {
			if(v[i] == 0) ret += i4.bitLength;
			else { ret += new i4(v[i]).numberOfTrailingZeros(); break; }
		}
		return ret;
	}
	@Override
	public int bitCount() {
		int ret = 0;
		for(int i = 0; i < mag.length; i++) ret += new i4(mag[i]).bitCount();
		if(sig < 0) ret += numberOfTrailingZerosUnsafe(mag) - 1;
		return ret;
	}
	@Override
	public boolean isPowerOf2() {
		if(sig <= 0) return false;
		var ret = new i4(mag[0]).isPowerOf2();
		for(int i = 1; ret && i < mag.length; i++) ret = mag[i] == 0;
		return ret;
	}
	@Override public boolean bit(int i) {  return shr(i).and(one()).signum() != 0; }
	@Override
	public bigint setBit(int i, boolean b) {
		var tmp = one().shl(i);
		return b ? or(tmp) : and(tmp.not());
	}
	@Override
	public int indexOfBit(int n) {
		if(n < 0) throw new IllegalArgumentException();
		var v = to2sComplementUnsafe();
		for(int i = v.length-1; i >= 0; i--) {
			var tmp = new i4(v[i]);
			var x = tmp.bitCount();
			if(n >= x) { n-=x; continue; }
			var j = tmp.indexOfBit(n);
			if(j < 0) throw new Error(errors.unreachable);
			return (v.length-1-i)*32 + j;
		}
		return sig >= 0 ? -1 : v.length*32+n;
	}
	
	
	
//	bitwise operations
	
	@Override public bigint and(bigint n) { return bitOp(n,(a,b)->a&b); }
	@Override public bigint or(bigint n) { return bitOp(n,(a,b)->a|b); }
	@Override public bigint xor(bigint n) { return bitOp(n,(a,b)->a^b); }
	@Override public bigint not() { return bitOp(this,(a,b)->~a); }
	private bigint bitOp(bigint x, BiFunction<Integer,Integer,Integer> f) {
		return new bigint(bitOp2sComplement(to2sComplementUnsafe(),x.to2sComplementUnsafe(),f));
	}
	private static int[] bitOp2sComplement(int[] a, int[] b, BiFunction<Integer,Integer,Integer> f) {
		if(a.length < b.length) { var tmp = a; a = b; b = tmp; }
		var ret = new int[a.length];
		int i = 0, m = b[0] < 0 ? ~0 : 0;
		for(int max = a.length-b.length; i < max; i++) ret[i] = f.apply(a[i],m);
		for(int j = 0; j < b.length; i++, j++) ret[i] = f.apply(a[i],b[j]);
		return ret;
	}
	@Override
	public bigint notWithoutSign() {
		if(sig == 0) return one();
		var mag = Arrays.copyOf(this.mag,this.mag.length);
		var z0 = new i4(mag[0]).numberOfLeadingZeros();
		mag[0] = ((~mag[0])<<z0) >>> z0;
		for(int i = 1; i < mag.length; i++) mag[i] = ~mag[i];
		return new bigint(sig,mag);
	}
	@Override
	public bigint shl(int amount) {
		if(amount <= 0) return amount == 0 ? this : amount == Long.MIN_VALUE ? zero() : shr(-amount);
		if(sig == 0) return this;
		int ni = shi(amount), nb = amount%32, _nb = 32-nb;
		var mag = Arrays.copyOf(this.mag,Math.toIntExact(this.mag.length+(long)ni+(nb!=0?1:0)));
		if(nb != 0) {
			mag[0] = this.mag[0] >>> _nb;
			for(int i = 1; i < this.mag.length; i++) {
				mag[i] = (this.mag[i-1] << nb) | (this.mag[i] >>> _nb);
			}
			mag[this.mag.length] = this.mag[this.mag.length-1] << nb;
		}
		return new bigint(sig,mag);
	}
	@Override
	public bigint shr(int x) {
		if(x <= 0) return x == 0 ? this : shl(-x);
		if(sig == 0) return this;
		int ni = shi(x), nb = x%32, _nb = 32-nb;
		if(ni >= mag.length) return new bigint(sig<0?-1:0);
		var mag = Arrays.copyOf(this.mag,this.mag.length-ni);
		if(nb != 0) {
			for(int i = mag.length-1; i > 0; i--) {
				mag[i] = (mag[i-1] << _nb) | (mag[i] >>> nb);
			}
			mag[0] = mag[0] >>> nb;
		}
		var ret = new bigint(sig,mag);
		if(sig < 0) { //shifts work a little different with negative numbers
			boolean lostOne = false;
			for(int i = this.mag.length-ni; i < this.mag.length && !lostOne; i++) {
				lostOne = this.mag[i] != 0;
			}
			if(!lostOne && nb != 0) {
				lostOne = (this.mag[this.mag.length-ni-1] << _nb) != 0;
			}
			if(lostOne) ret = ret.sub(new bigint(1));
		}
		return ret;
	}
	private static int shi(long x) {
		x /= 32; return (int)x == x ? (int)x : Integer.MAX_VALUE;
	}
	
//	Object overrides
	
	@Override public int hashCode() { return sig ^ Arrays.hashCode(mag); }
	@Override public boolean equals(Object obj) { return equals0(obj); }
	@Override
	public int compareTo(bigint n) {
		if(sig != n.sig) return sig < n.sig ? -1 : 1;
		return sig == 1 ? cmp(mag,n.mag) : sig == -1 ? cmp(n.mag,mag) : 0;
	}
	private static int cmp(int[] a, int[] b) {
		if(a.length < b.length) return -1;
		if(a.length > b.length) return 1;
		for(int i = 0; i < a.length; i++) {
			int cmp = Integer.compareUnsigned(a[i],b[i]);
			if(cmp != 0) return cmp;
		}
		return 0;
	}
	@Override public String toString() { return toString(FormatSpecifier.string); }
}
