package ring.math.numbers;
/* Copyright 2021 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Math.
 * 
 * Ring.Math is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Math is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Math.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

class ContainedClasses {
	static final Set<Class<?>> _i1 = toSet(i1.class,byte.class,Byte.class),			_i1s = union(_i1,i1s.class),				_u1 = toSet(u1.class),		_u1s = union(_u1,u1s.class);
	static final Set<Class<?>> _i2 = union(_i1,i2.class,short.class,Short.class),	_i2s = unionSet(_i1s,_i2,toSet(i2s.class)),	_u2 = union(_u1,u2.class),	_u2s = unionSet(_u2,_u1s,toSet(u2s.class));
	static final Set<Class<?>> _i4 = union(_i2,i4.class,int.class,Integer.class),	_i4s = unionSet(_i2s,_i4,toSet(i4s.class)),	_u4 = union(_u2,u4.class),	_u4s = unionSet(_u4,_u2s,toSet(u4s.class));
	static final Set<Class<?>> _i8 = union(_i4,i8.class,long.class,Long.class),		_i8s = unionSet(_i4s,_i8,toSet(i8s.class)),	_u8 = union(_u4,u8.class),	_u8s = unionSet(_u8,_u4s,toSet(u8s.class));
	static final Set<Class<?>> _f4 = toSet(f4.class,float.class,Float.class),		_f8 = union(_f4,f8.class,double.class,Double.class);
	static final Set<Class<?>> _bigint = unionSet(_i8,_u8,_i8s,_u8s,toSet(bigint.class));
	static final Set<Class<?>> _bigfloat = unionSet(_bigint,_f8,toSet(bigfloat.class));
	
	@SafeVarargs
	private static Set<Class<?>> unionSet(Set<Class<?>>...s) {
		var ret = new HashSet<Class<?>>();
		for(var e : s) ret.addAll(e);
		return Collections.unmodifiableSet(ret);
	}
	
	private static Set<Class<?>> union(Set<Class<?>> s, Class<?>...cls) {
		var ret = new HashSet<>(toSet(cls)); ret.addAll(s);
		return Collections.unmodifiableSet(ret);
	}
	
	private static Set<Class<?>> toSet(Class<?>...cls) {
		var ret = new HashSet<Class<?>>();
		for(var e : cls) ret.add(e);
		return Collections.unmodifiableSet(ret);
	}
}
